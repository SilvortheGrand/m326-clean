# Setup

1. Set correct __POSTGRES__ connection string in `appsettings.json` in `src/Server/`.
2. Update database for migrations inside `src/Database` execute `dotnet ef database update -s ..\Server`
3. Start project
4. Login with user `admin` and password `Admin@1234`