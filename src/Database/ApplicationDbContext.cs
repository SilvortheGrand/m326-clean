﻿using Database.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Database;

public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    public DbSet<Subject> Subjects { get; set; }
    public DbSet<CompetenceArea> CompetenceAreas { get; set; }
    public DbSet<Competence> Competences { get; set; }
    public DbSet<CompetenceResource> CompetenceResources { get; set; }
    public DbSet<CompetenceRemarks> CompetenceRemarks { get; set; }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<ApplicationUser>()
            .HasMany(x => x.CompletedCompetences)
            .WithMany(x => x.CompletedUsers);
        
        builder.AddIdentitySeeds();
    }
}