﻿namespace Database.Models;

public class Subject
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public List<CompetenceArea> CompetenceAreas { get; set; }
}