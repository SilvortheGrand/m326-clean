﻿namespace Database.Models;

public class CompetenceArea
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Subject Subject { get; set; }
    public List<Competence> Competences { get; set; }
}