﻿namespace Database.Models;

public class CompetenceRemarks
{
    public Guid Id { get; set; }
    public ApplicationUser Author { get; set; }
    public Competence Competence { get; set; }
    public string Content { get; set; }
}