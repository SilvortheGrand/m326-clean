﻿namespace Database.Models;

public enum DifficultyLevel
{
    Easy,
    Medium,
    Hard
}