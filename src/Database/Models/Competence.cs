﻿namespace Database.Models;

public class Competence
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public DifficultyLevel DifficultyLevel { get; set; }
    public CompetenceArea CompetenceArea { get; set; }
    public List<CompetenceResource> Resources { get; set; }
    public List<CompetenceRemarks> Remarks { get; set; }
    public List<ApplicationUser> CompletedUsers { get; set; } = new();
}