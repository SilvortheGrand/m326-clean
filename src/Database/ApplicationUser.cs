﻿using Database.Models;
using Microsoft.AspNetCore.Identity;

namespace Database;

public class ApplicationUser : IdentityUser
{
    public List<CompetenceRemarks> Remarks { get; set; } = new();
    public List<Competence> CompletedCompetences { get; set; } = new();
}