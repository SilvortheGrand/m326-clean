﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Database.Migrations
{
    /// <inheritdoc />
    public partial class AddedIdentitySeeding : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "0f91ed75-95a7-4e09-a998-341e8eee7679", "0f91ed75-95a7-4e09-a998-341e8eee7679", "School Management", "SCHOOL_MANAGEMENT" },
                    { "2b0f5679-dd26-494a-a55f-6414086d2540", "2b0f5679-dd26-494a-a55f-6414086d2540", "Teacher", "TEACHER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "01d6d801-ffe0-4809-9144-b37a171140f7", 0, "c7b13ef9-b2e3-478f-be17-abdbf37d259b", "admin@admin.com", true, false, null, "ADMIN@ADMIN.COM", "ADMIN", "AQAAAAIAAYagAAAAEPQQYFZqKS9Jjj8qiPjocZogXVi4fd0A0fnsmeoy+xbPnCgdl2t+FyQOI8j3KCq3Sg==", null, false, "9a9c95f8-227c-43da-a10a-eea196deb4ef", false, "admin" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "0f91ed75-95a7-4e09-a998-341e8eee7679", "01d6d801-ffe0-4809-9144-b37a171140f7" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2b0f5679-dd26-494a-a55f-6414086d2540");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "0f91ed75-95a7-4e09-a998-341e8eee7679", "01d6d801-ffe0-4809-9144-b37a171140f7" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0f91ed75-95a7-4e09-a998-341e8eee7679");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "01d6d801-ffe0-4809-9144-b37a171140f7");
        }
    }
}
