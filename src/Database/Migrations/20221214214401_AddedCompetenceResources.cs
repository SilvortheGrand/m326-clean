﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Database.Migrations
{
    /// <inheritdoc />
    public partial class AddedCompetenceResources : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DifficultyLevel",
                table: "Competences",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "CompetenceId",
                table: "CompetenceRemarks",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "CompetenceResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Url = table.Column<string>(type: "text", nullable: false),
                    CompetenceId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenceResources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetenceResources_Competences_CompetenceId",
                        column: x => x.CompetenceId,
                        principalTable: "Competences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "01d6d801-ffe0-4809-9144-b37a171140f7",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "093e531d-ee7d-4ea9-a6d0-348b577a7ea4", "AQAAAAIAAYagAAAAEEn14ZqWJVEzkF2W+vcOF1QLCXiXA722hx6ocUm5N1houVQs9mI9L2lfL4pfFzgoxA==", "3929ae66-3a3d-4853-9edc-7152a7c72d47" });

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceRemarks_CompetenceId",
                table: "CompetenceRemarks",
                column: "CompetenceId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceResources_CompetenceId",
                table: "CompetenceResources",
                column: "CompetenceId");

            migrationBuilder.AddForeignKey(
                name: "FK_CompetenceRemarks_Competences_CompetenceId",
                table: "CompetenceRemarks",
                column: "CompetenceId",
                principalTable: "Competences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompetenceRemarks_Competences_CompetenceId",
                table: "CompetenceRemarks");

            migrationBuilder.DropTable(
                name: "CompetenceResources");

            migrationBuilder.DropIndex(
                name: "IX_CompetenceRemarks_CompetenceId",
                table: "CompetenceRemarks");

            migrationBuilder.DropColumn(
                name: "DifficultyLevel",
                table: "Competences");

            migrationBuilder.DropColumn(
                name: "CompetenceId",
                table: "CompetenceRemarks");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "01d6d801-ffe0-4809-9144-b37a171140f7",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f8edd9ee-b5d2-40b8-95ef-7b1bb1a33a96", "AQAAAAIAAYagAAAAELSgr3Uc7soNCSnAJhkaDDn9O1iPHILFKV5zc+C9COlh1OGW2QnlNkrSHNZcu9ifww==", "19d21a45-8219-4fb2-abef-ce25c7d00941" });
        }
    }
}
