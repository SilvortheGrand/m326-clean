﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Database.Migrations
{
    /// <inheritdoc />
    public partial class ReworkedCompetenceDataModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserCompetence_AspNetUsers_AssignedUsersId",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserCompetence_Competences_CompetencesId",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropForeignKey(
                name: "FK_Competences_CompetenceCategories_CategoryId",
                table: "Competences");

            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "CompetenceCategories");

            migrationBuilder.DropTable(
                name: "DetailViews");

            migrationBuilder.DropTable(
                name: "CompetenceLists");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserCompetence",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationUserCompetence_CompetencesId",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropColumn(
                name: "DifficultyLevel",
                table: "Competences");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "Competences",
                newName: "CompetenceAreaId");

            migrationBuilder.RenameIndex(
                name: "IX_Competences_CategoryId",
                table: "Competences",
                newName: "IX_Competences_CompetenceAreaId");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "CompetenceAreas",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "CompetencesId",
                table: "ApplicationUserCompetence",
                newName: "CompletedCompetencesId");

            migrationBuilder.RenameColumn(
                name: "AssignedUsersId",
                table: "ApplicationUserCompetence",
                newName: "CompletedUsersId");

            migrationBuilder.AddColumn<Guid>(
                name: "SubjectId",
                table: "CompetenceAreas",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserCompetence",
                table: "ApplicationUserCompetence",
                columns: new[] { "CompletedCompetencesId", "CompletedUsersId" });

            migrationBuilder.CreateTable(
                name: "CompetenceRemarks",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorId = table.Column<string>(type: "text", nullable: true),
                    Content = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenceRemarks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetenceRemarks_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "01d6d801-ffe0-4809-9144-b37a171140f7",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "f8edd9ee-b5d2-40b8-95ef-7b1bb1a33a96", "AQAAAAIAAYagAAAAELSgr3Uc7soNCSnAJhkaDDn9O1iPHILFKV5zc+C9COlh1OGW2QnlNkrSHNZcu9ifww==", "19d21a45-8219-4fb2-abef-ce25c7d00941" });

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceAreas_SubjectId",
                table: "CompetenceAreas",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserCompetence_CompletedUsersId",
                table: "ApplicationUserCompetence",
                column: "CompletedUsersId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceRemarks_AuthorId",
                table: "CompetenceRemarks",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserCompetence_AspNetUsers_CompletedUsersId",
                table: "ApplicationUserCompetence",
                column: "CompletedUsersId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserCompetence_Competences_CompletedCompetencesId",
                table: "ApplicationUserCompetence",
                column: "CompletedCompetencesId",
                principalTable: "Competences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetenceAreas_Subjects_SubjectId",
                table: "CompetenceAreas",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Competences_CompetenceAreas_CompetenceAreaId",
                table: "Competences",
                column: "CompetenceAreaId",
                principalTable: "CompetenceAreas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserCompetence_AspNetUsers_CompletedUsersId",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUserCompetence_Competences_CompletedCompetencesId",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetenceAreas_Subjects_SubjectId",
                table: "CompetenceAreas");

            migrationBuilder.DropForeignKey(
                name: "FK_Competences_CompetenceAreas_CompetenceAreaId",
                table: "Competences");

            migrationBuilder.DropTable(
                name: "CompetenceRemarks");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.DropIndex(
                name: "IX_CompetenceAreas_SubjectId",
                table: "CompetenceAreas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationUserCompetence",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationUserCompetence_CompletedUsersId",
                table: "ApplicationUserCompetence");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "CompetenceAreas");

            migrationBuilder.RenameColumn(
                name: "CompetenceAreaId",
                table: "Competences",
                newName: "CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Competences_CompetenceAreaId",
                table: "Competences",
                newName: "IX_Competences_CategoryId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "CompetenceAreas",
                newName: "Title");

            migrationBuilder.RenameColumn(
                name: "CompletedUsersId",
                table: "ApplicationUserCompetence",
                newName: "AssignedUsersId");

            migrationBuilder.RenameColumn(
                name: "CompletedCompetencesId",
                table: "ApplicationUserCompetence",
                newName: "CompetencesId");

            migrationBuilder.AddColumn<int>(
                name: "DifficultyLevel",
                table: "Competences",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationUserCompetence",
                table: "ApplicationUserCompetence",
                columns: new[] { "AssignedUsersId", "CompetencesId" });

            migrationBuilder.CreateTable(
                name: "CompetenceLists",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CompetenceAreaId = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenceLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetenceLists_CompetenceAreas_CompetenceAreaId",
                        column: x => x.CompetenceAreaId,
                        principalTable: "CompetenceAreas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DetailViews",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetailViews", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompetenceCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CompetenceListId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenceCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetenceCategories_CompetenceLists_CompetenceListId",
                        column: x => x.CompetenceListId,
                        principalTable: "CompetenceLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorId = table.Column<string>(type: "text", nullable: false),
                    CompetenceDetailViewId = table.Column<Guid>(type: "uuid", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comment_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comment_DetailViews_CompetenceDetailViewId",
                        column: x => x.CompetenceDetailViewId,
                        principalTable: "DetailViews",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "01d6d801-ffe0-4809-9144-b37a171140f7",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "27ab053e-bff5-4c80-9b6f-98b8067e842b", "AQAAAAIAAYagAAAAEPTr4EhFUO+7XpuuWJ4kPIgVz5tFkgnqXMEcLkGwVera/Kfrsgt7kVVYdAmzfFIZcQ==", "5a54726b-5f2a-44a9-8d1a-7dbc20fa45a7" });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserCompetence_CompetencesId",
                table: "ApplicationUserCompetence",
                column: "CompetencesId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_AuthorId",
                table: "Comment",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_CompetenceDetailViewId",
                table: "Comment",
                column: "CompetenceDetailViewId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceCategories_CompetenceListId",
                table: "CompetenceCategories",
                column: "CompetenceListId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceLists_CompetenceAreaId",
                table: "CompetenceLists",
                column: "CompetenceAreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserCompetence_AspNetUsers_AssignedUsersId",
                table: "ApplicationUserCompetence",
                column: "AssignedUsersId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUserCompetence_Competences_CompetencesId",
                table: "ApplicationUserCompetence",
                column: "CompetencesId",
                principalTable: "Competences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Competences_CompetenceCategories_CategoryId",
                table: "Competences",
                column: "CategoryId",
                principalTable: "CompetenceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
