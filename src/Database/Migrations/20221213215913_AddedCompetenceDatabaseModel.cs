﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Database.Migrations
{
    /// <inheritdoc />
    public partial class AddedCompetenceDatabaseModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CompetenceAreas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenceAreas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DetailViews",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetailViews", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompetenceLists",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false),
                    CompetenceAreaId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenceLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetenceLists_CompetenceAreas_CompetenceAreaId",
                        column: x => x.CompetenceAreaId,
                        principalTable: "CompetenceAreas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: false),
                    CompetenceDetailViewId = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comment_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comment_DetailViews_CompetenceDetailViewId",
                        column: x => x.CompetenceDetailViewId,
                        principalTable: "DetailViews",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompetenceCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CompetenceListId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetenceCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompetenceCategories_CompetenceLists_CompetenceListId",
                        column: x => x.CompetenceListId,
                        principalTable: "CompetenceLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Competences",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    DifficultyLevel = table.Column<int>(type: "integer", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Competences", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Competences_CompetenceCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "CompetenceCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationUserCompetence",
                columns: table => new
                {
                    AssignedUsersId = table.Column<string>(type: "text", nullable: false),
                    CompetencesId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUserCompetence", x => new { x.AssignedUsersId, x.CompetencesId });
                    table.ForeignKey(
                        name: "FK_ApplicationUserCompetence_AspNetUsers_AssignedUsersId",
                        column: x => x.AssignedUsersId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicationUserCompetence_Competences_CompetencesId",
                        column: x => x.CompetencesId,
                        principalTable: "Competences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "01d6d801-ffe0-4809-9144-b37a171140f7",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "27ab053e-bff5-4c80-9b6f-98b8067e842b", "AQAAAAIAAYagAAAAEPTr4EhFUO+7XpuuWJ4kPIgVz5tFkgnqXMEcLkGwVera/Kfrsgt7kVVYdAmzfFIZcQ==", "5a54726b-5f2a-44a9-8d1a-7dbc20fa45a7" });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserCompetence_CompetencesId",
                table: "ApplicationUserCompetence",
                column: "CompetencesId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_AuthorId",
                table: "Comment",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_CompetenceDetailViewId",
                table: "Comment",
                column: "CompetenceDetailViewId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceCategories_CompetenceListId",
                table: "CompetenceCategories",
                column: "CompetenceListId");

            migrationBuilder.CreateIndex(
                name: "IX_CompetenceLists_CompetenceAreaId",
                table: "CompetenceLists",
                column: "CompetenceAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_Competences_CategoryId",
                table: "Competences",
                column: "CategoryId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationUserCompetence");

            migrationBuilder.DropTable(
                name: "Comment");

            migrationBuilder.DropTable(
                name: "Competences");

            migrationBuilder.DropTable(
                name: "DetailViews");

            migrationBuilder.DropTable(
                name: "CompetenceCategories");

            migrationBuilder.DropTable(
                name: "CompetenceLists");

            migrationBuilder.DropTable(
                name: "CompetenceAreas");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "01d6d801-ffe0-4809-9144-b37a171140f7",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c7b13ef9-b2e3-478f-be17-abdbf37d259b", "AQAAAAIAAYagAAAAEPQQYFZqKS9Jjj8qiPjocZogXVi4fd0A0fnsmeoy+xbPnCgdl2t+FyQOI8j3KCq3Sg==", "9a9c95f8-227c-43da-a10a-eea196deb4ef" });
        }
    }
}
