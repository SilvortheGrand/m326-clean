﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Database;

public static class DataSeeder
{
    public static ModelBuilder AddIdentitySeeds(this ModelBuilder builder)
    {
        const string managementRoleId = "0f91ed75-95a7-4e09-a998-341e8eee7679";
        const string teacherRoleId = "2b0f5679-dd26-494a-a55f-6414086d2540";

        const string adminUserId = "01d6d801-ffe0-4809-9144-b37a171140f7";

        builder.Entity<IdentityRole>()
            .HasData(new List<IdentityRole>
            {
                new()
                {
                    Id = managementRoleId,
                    Name = "School Management",
                    NormalizedName = "SCHOOL_MANAGEMENT",
                    ConcurrencyStamp = managementRoleId
                },
                new()
                {
                    Id = teacherRoleId,
                    Name = "Teacher",
                    NormalizedName = "TEACHER",
                    ConcurrencyStamp = teacherRoleId
                }
            });

        var appUser = new ApplicationUser
        {
            Id = adminUserId,
            Email = "admin@admin.com",
            EmailConfirmed = true,
            UserName = "admin",
            NormalizedEmail = "ADMIN@ADMIN.COM",
            NormalizedUserName = "ADMIN"
        };

        var passwordHasher = new PasswordHasher<ApplicationUser>();
        appUser.PasswordHash = passwordHasher.HashPassword(appUser, "Admin@1234");

        builder.Entity<ApplicationUser>().HasData(appUser);

        builder.Entity<IdentityUserRole<string>>()
            .HasData(new IdentityUserRole<string> {RoleId = managementRoleId, UserId = adminUserId});

        return builder;
    }
}