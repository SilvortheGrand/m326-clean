﻿namespace Shared.Auth;

public record AuthSuccessResponse(string Token);
public record GetCurrentUserResponse(string Id, string Username);