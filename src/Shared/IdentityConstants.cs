﻿namespace Shared;

public class IdentityConstants
{
    public const string ManagementRole = "School Management";
    public const string TeacherRole = "Teacher";
}