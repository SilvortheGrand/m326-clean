﻿namespace Shared.Competences;

public record  GetCompetenceResponse(string Id, string Name, string Description, SharedDifficultyLevel DifficultyLevel, List<GetCompetenceResourceResponse> Resources, List<GetCompetenceRemarksResponse> Remarks);
public record CreateCompetenceRequest(string AreaId, string Name, string Description, SharedDifficultyLevel DifficultyLevel);
public record UpdateCompetenceRequest(string Id, string NewName, string NewDescription, SharedDifficultyLevel NewDifficultyLevel);

public record GetCompetenceResourceResponse(string Id, string Name, string Url);
public record CreateCompetenceResourceRequest(string CompetenceId, string Name, string Url);

public record GetCompetenceRemarksResponse(string Id, string Content, UserResponse Author);
public record CreateCompetenceRemarksRequest(string CompetenceId, string Content);