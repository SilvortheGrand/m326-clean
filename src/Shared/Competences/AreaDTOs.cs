﻿namespace Shared.Competences;

public record GetCompetenceAreaResponse(string Id, string Name, List<GetCompetenceResponse> Competences);
public record CreateCompetenceAreaRequest(string SubjectId, string Name);
public record UpdateCompetenceAreaRequest(string AreaId, string Name);