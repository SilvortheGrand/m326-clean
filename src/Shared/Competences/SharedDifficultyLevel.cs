﻿namespace Shared.Competences;

public enum SharedDifficultyLevel
{
    Easy,
    Medium,
    Hard
}