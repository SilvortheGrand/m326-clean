﻿namespace Shared.Competences;

public record GetSubjectOverviewResponse(string Id, string Name, int AreaCount, int CompetenceCount);
public record GetSubjectResponse(string Id, string Name, List<GetCompetenceAreaResponse> Areas);
public record CreateSubjectRequest(string Name) : ICompetenceDto;
public record UpdateSubjectRequest(string Id, string Name);