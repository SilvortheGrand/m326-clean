﻿namespace Shared;

public record UserResponse(string Id, string Username);