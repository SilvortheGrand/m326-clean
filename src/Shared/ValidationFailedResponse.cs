﻿namespace Shared;

public record ValidationFailedResponse(IEnumerable<string> Errors);