﻿namespace Server.Extensions;

public static class HttpContextExtensions
{
    private const string RefreshTokenCookieName = "refreshToken";

    private static readonly CookieOptions CookieOptions = new()
    {
        HttpOnly = true,
        Expires = DateTimeOffset.UtcNow.AddDays(7),
        Secure = true
    };

    public static void SetRefreshTokenCookie(this HttpResponse response, string refreshToken)
    {
        response.Cookies.Append(RefreshTokenCookieName, refreshToken, CookieOptions);
    }

    public static bool TryGetRefreshTokenCookie(this HttpRequest request, out string? refreshToken)
    {
        return request.Cookies.TryGetValue(RefreshTokenCookieName, out refreshToken);
    }

    public static void RemoveRefreshTokenCookie(this HttpResponse response)
    {
        response.Cookies.Delete(RefreshTokenCookieName);
    }
}