﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class CreateCompetenceAreaRequestValidator : AbstractValidator<CreateCompetenceAreaRequest>
{
    public CreateCompetenceAreaRequestValidator()
    {
        RuleFor(x => x.SubjectId)
            .NotEmpty().WithMessage("Subject id cannot be empty")
            .Must(x => Guid.TryParse(x, out _));
        RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot be empty");
    }    
}