﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class UpdateCompetenceAreaRequestValidator : AbstractValidator<UpdateCompetenceAreaRequest>
{
    public UpdateCompetenceAreaRequestValidator()
    {
        RuleFor(x => x.AreaId).NotEmpty().WithMessage("Area id cannot be empty.");
        RuleFor(x => x.Name).NotEmpty().WithMessage("New name cannot be empty.");
    }
}