﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class CreateCompetenceRemarksRequestValidator : AbstractValidator<CreateCompetenceRemarksRequest>
{
    public CreateCompetenceRemarksRequestValidator()
    {
        RuleFor(x => x.CompetenceId)
            .NotEmpty().WithMessage("Competence Id cannot be empty")
            .Must(x => Guid.TryParse(x, out _)).WithMessage("Not a valid Id");
        RuleFor(x => x.Content)
            .NotEmpty().WithMessage("Content cannot be empty");
    }
}