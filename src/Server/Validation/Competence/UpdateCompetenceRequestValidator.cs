﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class UpdateCompetenceRequestValidator : AbstractValidator<UpdateCompetenceRequest>
{
    public UpdateCompetenceRequestValidator()
    {
        RuleFor(x => x.Id)
            .NotEmpty().WithMessage("Id cannot be empty.")
            .Must(x => Guid.TryParse(x, out _));
        RuleFor(x => x.NewName)
            .NotEmpty().WithMessage("New name cannot be empty");
        RuleFor(x => x.NewDescription)
            .NotEmpty().WithMessage("New description cannot be empty");
        RuleFor(x => x.NewDifficultyLevel)
            .NotNull().WithMessage("New difficulty cannot be empty");
    }
}