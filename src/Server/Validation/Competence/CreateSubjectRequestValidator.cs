﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class CreateSubjectRequestValidator : AbstractValidator<CreateSubjectRequest>
{
    public CreateSubjectRequestValidator()
    {
        RuleFor(x => x.Name).NotEmpty().WithMessage("Area name cannot be empty.");
    }
}