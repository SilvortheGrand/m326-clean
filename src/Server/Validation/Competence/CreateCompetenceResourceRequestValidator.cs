﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class CreateCompetenceResourceRequestValidator : AbstractValidator<CreateCompetenceResourceRequest>
{
    public CreateCompetenceResourceRequestValidator()
    {
        RuleFor(x => x.CompetenceId)
            .NotEmpty().WithMessage("Competence Id cannot be empty")
            .Must(x => Guid.TryParse(x, out _)).WithMessage("Not a valid competence id");
        RuleFor(x => x.Name)
            .NotEmpty().WithMessage("Name cannot be empty");
        RuleFor(x => x.Url)
            .NotEmpty().WithMessage("Url cannot be empty");
    }
}