﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class UpdateSubjectRequestValidator : AbstractValidator<UpdateSubjectRequest>
{
    public UpdateSubjectRequestValidator()
    {
        RuleFor(x => x.Id)
            .NotEmpty().WithMessage("Id cannot be empty")
            .Must(x => Guid.TryParse(x, out _));
        RuleFor(x => x.Name).NotEmpty().WithMessage("New name cannot be empty");
    }
}