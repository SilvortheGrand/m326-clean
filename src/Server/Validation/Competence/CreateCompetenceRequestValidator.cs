﻿using FluentValidation;
using Shared.Competences;

namespace Server.Validation.Competence;

public class CreateCompetenceRequestValidator : AbstractValidator<CreateCompetenceRequest>
{
    public CreateCompetenceRequestValidator()
    {
        RuleFor(x => x.AreaId)
            .NotEmpty().WithMessage("Area id cannot be empty")
            .Must(x => Guid.TryParse(x, out _));
        RuleFor(x => x.Name).NotEmpty().WithMessage("Competence name cannot be empty.");
        RuleFor(x => x.Description).NotEmpty().WithMessage("Description cannot be empty.");
    }   
}