﻿using FluentValidation;
using Shared.Auth;

namespace Server.Validation;

public class RegisterRequestValidator : AbstractValidator<RegisterRequest>
{
    public RegisterRequestValidator()
    {
        RuleFor(x => x.Username)
            .NotEmpty().WithMessage("Username cannot be empty.");
        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Email cannot be empty.")
            .EmailAddress().WithMessage("Not a valid email address.");
        RuleFor(x => x.Password)
            .NotEmpty().WithMessage("Password cannot be empty.");
        RuleFor(x => x.PasswordConfirm)
            .NotEmpty().WithMessage("Confirmation password cannot be empty.")
            .Equal(x => x.Password).WithMessage("Passwords do not match.");
    }
}