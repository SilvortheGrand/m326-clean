﻿using AutoMapper;
using Database;
using Domain.Entities;

namespace Server.Mapping;

public class UserMapping : Profile
{
    public UserMapping()
    {
        CreateMap<ApplicationUser, User>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Username, o => o.MapFrom(src => src.UserName))
            .ForMember(dest => dest.Email, o => o.MapFrom(src => src.Email))
            .ForMember(dest => dest.Remarks, o => o.MapFrom(src => src.Remarks))
            .ForMember(dest => dest.CompletedCompetences, o => o.MapFrom(src => src.CompletedCompetences));
    }
}