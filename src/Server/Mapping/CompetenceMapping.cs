﻿using AutoMapper;
using Database.Models;
using Domain.Entities.Competences;

namespace Server.Mapping;

public class CompetenceMapping : Profile
{
    public CompetenceMapping()
    {
        CreateMap<Subject, DomainSubject>()
            .ForCtorParam(nameof(DomainSubject.Id), o => o.MapFrom(src => src.Id))
            .ForCtorParam(nameof(DomainSubject.Name), o => o.MapFrom(src => src.Name))
            .ForCtorParam(nameof(DomainSubject.Areas), o => o.MapFrom(src => src.CompetenceAreas));

        CreateMap<CompetenceArea, DomainCompetenceArea>()
            .ForCtorParam(nameof(DomainCompetenceArea.Id), o => o.MapFrom(src => src.Id))
            .ForCtorParam(nameof(DomainCompetenceArea.Name), o => o.MapFrom(src => src.Name))
            .ForCtorParam(nameof(DomainCompetenceArea.Competences), o => o.MapFrom(src => src.Competences));

        CreateMap<Competence, DomainCompetence>()
            .ForCtorParam(nameof(DomainCompetence.Id), o => o.MapFrom(src => src.Id))
            .ForCtorParam(nameof(DomainCompetence.Name), o => o.MapFrom(src => src.Name))
            .ForCtorParam(nameof(DomainCompetence.Description), o => o.MapFrom(src => src.Description))
            .ForCtorParam(nameof(DomainCompetence.DifficultyLevel), o => o.MapFrom(src => src.DifficultyLevel))
            .ForCtorParam(nameof(DomainCompetence.Resources), o => o.MapFrom(src => src.Resources))
            .ForCtorParam(nameof(DomainCompetence.Remarks), o => o.MapFrom(src => src.Remarks))
            .ForCtorParam(nameof(DomainCompetence.CompletedUsers), o => o.MapFrom(src => src.CompletedUsers));

        CreateMap<CompetenceResource, DomainCompetenceResource>()
            .ForCtorParam(nameof(DomainCompetenceResource.Id), o => o.MapFrom(src => src.Id))
            .ForCtorParam(nameof(DomainCompetenceResource.Name), o => o.MapFrom(src => src.Name))
            .ForCtorParam(nameof(DomainCompetenceResource.Url), o => o.MapFrom(src => src.Url));

        CreateMap<CompetenceRemarks, DomainCompetenceRemarks>()
            .ForCtorParam(nameof(DomainCompetenceRemarks.Id), o => o.MapFrom(src => src.Id))
            .ForCtorParam(nameof(DomainCompetenceRemarks.Content), o => o.MapFrom(src => src.Content))
            .ForCtorParam(nameof(DomainCompetenceRemarks.Author), o => o.MapFrom(src => src.Author));
    }
}