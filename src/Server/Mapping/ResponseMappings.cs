﻿using AutoMapper;
using Domain.Entities;
using Domain.Entities.Competences;
using Shared;
using Shared.Competences;

namespace Server.Mapping;

public class ResponseMappings : Profile
{
    public ResponseMappings()
    {
        CreateMap<DomainSubjectInfo, GetSubjectOverviewResponse>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.AreaCount, o => o.MapFrom(src => src.AreaCount))
            .ForMember(dest => dest.CompetenceCount, o => o.MapFrom(src => src.CompetenceCount));
        
        CreateMap<DomainSubject, GetSubjectResponse>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Areas, o => o.MapFrom(src => src.Areas));

        CreateMap<DomainCompetenceArea, GetCompetenceAreaResponse>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Competences, o => o.MapFrom(src => src.Competences));

        CreateMap<DomainCompetence, GetCompetenceResponse>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.DifficultyLevel, o => o.MapFrom(src => src.DifficultyLevel))
            .ForMember(dest => dest.Description, o => o.MapFrom(src => src.Description))
            .ForMember(dest => dest.Remarks, o => o.MapFrom(src => src.Remarks))
            .ForMember(dest => dest.Resources, o => o.MapFrom(src => src.Resources));

        CreateMap<DomainCompetenceRemarks, GetCompetenceRemarksResponse>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Content, o => o.MapFrom(src => src.Content))
            .ForMember(dest => dest.Author, o => o.MapFrom(src => src.Author));

        CreateMap<DomainCompetenceResource, GetCompetenceResourceResponse>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Url, o => o.MapFrom(src => src.Url));

        CreateMap<User, UserResponse>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Username, o => o.MapFrom(src => src.Username));
    }
}