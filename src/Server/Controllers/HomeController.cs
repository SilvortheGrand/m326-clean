﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Authorize]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult Test()
        {
            return Ok("This works!");
        }
    }
}
