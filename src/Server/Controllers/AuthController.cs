﻿using Client.Errors;
using Domain.Errors;
using Domain.Errors.Auth;
using FluentResults;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Extensions;
using Server.Services;
using Shared;
using Shared.Auth;
using Swashbuckle.AspNetCore.Annotations;
using DuplicateUserError = Domain.Errors.Auth.DuplicateUserError;

namespace Server.Controllers;

public class AuthController : Controller
{
    private readonly IAuthenticationService _authenticationService;
    private readonly IValidationService _validationService;

    public AuthController(IAuthenticationService authenticationService, IValidationService validationService)
    {
        _authenticationService = authenticationService;
        _validationService = validationService;
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Authentication Token", typeof(AuthSuccessResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(Dictionary<string, string[]>))]
    [SwaggerResponse(StatusCodes.Status401Unauthorized, "Authentication failed")]
    public async Task<IActionResult> Login([FromBody] LoginRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _authenticationService.LoginAsync(request.Username, request.Password);

        if (result.IsFailed)
            return result.HasError<InvalidCredentialError>()
                ? Unauthorized()
                : StatusCode(StatusCodes.Status500InternalServerError);

        Response.SetRefreshTokenCookie(result.Value.RefreshToken);
        return Ok(new AuthSuccessResponse(result.Value.Token));
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Authentication token", typeof(AuthSuccessResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(Dictionary<string, string[]>))]
    [SwaggerResponse(StatusCodes.Status409Conflict, "User already exists")]
    public async Task<IActionResult> Register([FromBody] RegisterRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _authenticationService.RegisterAsync(request.Username, request.Email, request.Password,
            request.PasswordConfirm);

        if (result.HasError<DuplicateUserError>())
            return Conflict();

        if (result.HasError<UserCreationError>())
            return BadRequest(new ValidationFailedResponse(result.Errors.Select(x => x.Message)));

        if (result.IsFailed) return StatusCode(StatusCodes.Status500InternalServerError);

        Response.SetRefreshTokenCookie(result.Value.RefreshToken);
        return Ok(new AuthSuccessResponse(result.Value.Token));
    }

    [HttpPost]
    [Authorize]
    [SwaggerResponse(StatusCodes.Status200OK)]
    public async Task<IActionResult> Logout()
    {
        await _authenticationService.LogoutAsync(User);
        Response.RemoveRefreshTokenCookie();
        return Ok();
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK)]
    [SwaggerResponse(StatusCodes.Status401Unauthorized, "Refresh token not present.")]
    public async Task<IActionResult> RefreshToken()
    {
        if (!Request.TryGetRefreshTokenCookie(out var refreshToken) || string.IsNullOrEmpty(refreshToken))
            return Unauthorized();

        var result = await _authenticationService.RefreshTokenAsync(refreshToken);
        if (result.IsFailed)
            return Unauthorized();

        Response.SetRefreshTokenCookie(result.Value.RefreshToken);
        return Ok(new AuthSuccessResponse(result.Value.Token));
    }

    [HttpGet]
    [Authorize]
    [SwaggerResponse(StatusCodes.Status200OK, "Current user data", typeof(GetCurrentUserResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "User could not be fetched.", typeof(string[]))]
    public async Task<IActionResult> GetCurrentUser()
    {
        var result = await _authenticationService.GetCurrentUserAsync(User);
        if (result.IsFailed)
            return BadRequest(result.Errors.Select(x => x.Message));

        return Ok(new GetCurrentUserResponse(result.Value.Id.ToString(), result.Value.Username));
    }
}