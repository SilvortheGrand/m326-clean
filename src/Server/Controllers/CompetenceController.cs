﻿using AutoMapper;
using Client.Errors;
using Domain.Entities.Competences;
using Domain.Errors.Competences;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Shared;
using Shared.Competences;
using Swashbuckle.AspNetCore.Annotations;

namespace Server.Controllers;

[Authorize]
public class CompetenceController : Controller
{
    private readonly ICompetenceService _competenceService;
    private readonly IValidationService _validationService;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public CompetenceController(ICompetenceService competenceService, IMapper mapper, IUserService userService, IValidationService validationService)
    {
        _competenceService = competenceService;
        _mapper = mapper;
        _userService = userService;
        _validationService = validationService;
    }

    [HttpGet]
    public async Task<IActionResult> GetSubjectOverview()
    {
        var result = await _competenceService.GetSubjectOverviewAsync();

        return Ok(_mapper.Map<IEnumerable<GetSubjectOverviewResponse>>(result.Value));
    }
    
    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, "Subject", typeof(GetSubjectResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Subject not found")]
    public async Task<IActionResult> GetSubject(string id)
    {
        if (!Guid.TryParse(id, out var guidId))
            return BadRequest();

        var result = await _competenceService.GetSubjectAsync(guidId);
        if (result.HasError<NotFoundError<DomainSubject>>())
            return NotFound();

        return Ok(_mapper.Map<GetSubjectResponse>(result.Value));
    }
    
    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, "Competence area", typeof(GetCompetenceAreaResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Area not found")]
    public async Task<IActionResult> GetArea(string id)
    {
        if (!Guid.TryParse(id, out var guidId))
            return BadRequest();

        var result = await _competenceService.GetAreaAsync(guidId);
        if (result.HasError<NotFoundError<DomainCompetenceArea>>())
            return NotFound();

        return Ok(_mapper.Map<GetCompetenceAreaResponse>(result.Value));
    }

    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, "Competence", typeof(GetCompetenceResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid Id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Competence not found")]
    public async Task<IActionResult> GetCompetence(string id)
    {
        if (!Guid.TryParse(id, out var guidId))
            return BadRequest();

        var result = await _competenceService.GetCompetenceAsync(guidId);
        if (result.HasError<NotFoundError<DomainCompetence>>())
            return NotFound();

        return Ok(_mapper.Map<GetCompetenceResponse>(result.Value));
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Remarks created", typeof(GetCompetenceRemarksResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(ValidationFailedResponse))]
    [SwaggerResponse(StatusCodes.Status401Unauthorized, "User not found")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Competence not found")]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, "Creation error")]
    public async Task<IActionResult> CreateCompetenceRemarks([FromBody] CreateCompetenceRemarksRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var userResult = await _userService.GetUserFromClaimsPrincipalAsync(User);
        if (userResult.IsFailed)
            return Unauthorized();
        
        var result = await _competenceService.CreateRemarkAsync(Guid.Parse(request.CompetenceId), request.Content, userResult.Value.Id);

        if (result.HasError<NotFoundError<DomainCompetence>>())
            return NotFound();

        if (result.HasError<CreationError<DomainCompetenceRemarks>>())
            return StatusCode(StatusCodes.Status500InternalServerError);

        return Ok(_mapper.Map<GetCompetenceRemarksResponse>(result.Value));
    }
    
    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, "Resource", typeof(GetCompetenceResourceResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Resource not found")]
    public async Task<IActionResult> GetResource(string id)
    {
        if (!Guid.TryParse(id, out var guidId))
            return BadRequest();

        var result = await _competenceService.GetResourceAsync(guidId);
        if (result.HasError<NotFoundError<DomainCompetenceResource>>())
            return NotFound();

        return Ok(_mapper.Map<GetCompetenceResourceResponse>(result.Value));
    }
    
    [HttpGet]
    [SwaggerResponse(StatusCodes.Status200OK, "Remarks", typeof(GetCompetenceRemarksResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Remarks not found")]
    public async Task<IActionResult> GetRemarks(string id)
    {
        if (!Guid.TryParse(id, out var guidId))
            return BadRequest();

        var result = await _competenceService.GetRemarksAsync(guidId);
        if (result.HasError<NotFoundError<DomainCompetenceRemarks>>())
            return NotFound();

        return Ok(_mapper.Map<GetCompetenceRemarksResponse>(result.Value));
    }

    [HttpDelete]
    [SwaggerResponse(StatusCodes.Status200OK, "Remark deleted")]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid Id")]
    [SwaggerResponse(StatusCodes.Status401Unauthorized, "Deletion unauthorized.")]
    public async Task<IActionResult> DeleteRemarks(string id)
    {
        if (!Guid.TryParse(id, out var guidId))
            return BadRequest();

        var userResult = await _userService.GetUserFromClaimsPrincipalAsync(User);
        if (userResult.IsFailed)
            return Unauthorized();

        
        var result = await _competenceService.DeleteRemarksAsync(guidId, userResult.Value.Id);
        if (result.HasError<UnauthorizedError>())
            return Unauthorized();
        
        return Ok();
    }
}