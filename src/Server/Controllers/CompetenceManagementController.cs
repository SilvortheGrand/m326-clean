﻿using AutoMapper;
using Domain.Entities.Competences;
using Domain.Errors.Competences;
using FluentResults;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Auth;
using Server.Services;
using Shared;
using Shared.Competences;
using Swashbuckle.AspNetCore.Annotations;

namespace Server.Controllers;

[Authorize(Policy = AuthenticationDefaults.ManagementPolicyName)]
public class CompetenceManagementController : Controller
{
    private readonly ICompetenceService _competenceService;
    private readonly IUserService _userService;
    private readonly IValidationService _validationService;
    private readonly IMapper _mapper;

    public CompetenceManagementController(ICompetenceService competenceService, IValidationService validationService,
        IMapper mapper, IUserService userService)
    {
        _competenceService = competenceService;
        _validationService = validationService;
        _mapper = mapper;
        _userService = userService;
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Subject created", typeof(GetSubjectResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(ValidationFailedResponse))]
    [SwaggerResponse(StatusCodes.Status409Conflict, "Duplicate subject name")]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, "Creation error")]
    public async Task<IActionResult> CreateSubject([FromBody] CreateSubjectRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _competenceService.CreateSubjectAsync(request.Name);

        if (result.HasError<DuplicateError<DomainSubject>>())
            return Conflict();

        if (result.HasError<CreationError<DomainSubject>>())
            return StatusCode(StatusCodes.Status500InternalServerError);

        return Ok(_mapper.Map<GetSubjectResponse>(result.Value));
    }

    [HttpDelete]
    [SwaggerResponse(StatusCodes.Status200OK, "Subject deleted")]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid Id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Subject not found")]
    public async Task<IActionResult> DeleteSubject(string id)
    {
        if (!Guid.TryParse(id, out var idGuid))
            return BadRequest();

        var result = await _competenceService.DeleteSubjectAsync(idGuid);

        if (result.HasError<NotFoundError<DomainSubject>>())
            return NotFound();

        return Ok();
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Subject updated", typeof(GetSubjectResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(ValidationFailedResponse))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Subject not found")]
    public async Task<IActionResult> UpdateSubject([FromBody] UpdateSubjectRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _competenceService.UpdateSubjectAsync(Guid.Parse(request.Id), request.Name);

        if (result.HasError<NotFoundError<DomainSubject>>())
            return NotFound();

        return Ok(_mapper.Map<GetSubjectResponse>(result.Value));
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Area created", typeof(GetCompetenceAreaResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(ValidationFailedResponse))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Subject not found")]
    [SwaggerResponse(StatusCodes.Status409Conflict, "Area already exists")]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, "Creation error")]
    public async Task<IActionResult> CreateCompetenceArea([FromBody] CreateCompetenceAreaRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _competenceService.CreateAreaAsync(Guid.Parse(request.SubjectId), request.Name);

        if (result.HasError<NotFoundError<DomainSubject>>())
            return NotFound();

        if (result.HasError<DuplicateError<DomainCompetenceArea>>())
            return Conflict();

        if (result.HasError<CreationError<DomainCompetenceArea>>())
            return StatusCode(StatusCodes.Status500InternalServerError);

        return Ok(_mapper.Map<GetCompetenceAreaResponse>(result.Value));
    }

    [HttpDelete]
    [SwaggerResponse(StatusCodes.Status200OK, "Area deleted")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Area not found")]
    public async Task<IActionResult> DeleteCompetenceArea(string id)
    {
        if (!Guid.TryParse(id, out var idGuid))
            return BadRequest();

        var result = await _competenceService.DeleteAreaAsync(idGuid);

        if (result.HasError<NotFoundError<DomainCompetenceArea>>())
            return NotFound();

        return Ok();
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Area updated", typeof(GetCompetenceAreaResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(ValidationFailedResponse))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Area not found")]
    public async Task<IActionResult> UpdateCompetenceArea([FromBody] UpdateCompetenceAreaRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _competenceService.UpdateAreaAsync(Guid.Parse(request.AreaId), request.Name);

        if (result.HasError<NotFoundError<DomainCompetenceArea>>())
            return NotFound();

        return Ok(_mapper.Map<GetCompetenceAreaResponse>(result.Value));
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Competence created", typeof(GetCompetenceResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(ValidationFailedResponse))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Area not found")]
    [SwaggerResponse(StatusCodes.Status409Conflict, "Duplicate competence name")]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, "Creation error")]
    public async Task<IActionResult> CreateCompetence([FromBody] CreateCompetenceRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _competenceService.CreateCompetenceAsync(Guid.Parse(request.AreaId), request.Name,
            request.Description, _mapper.Map<DomainDifficultyLevel>(request.DifficultyLevel));

        if (result.HasError<NotFoundError<DomainCompetenceArea>>())
            return NotFound();

        if (result.HasError<DuplicateError<DomainCompetence>>())
            return Conflict();

        if (result.HasError<CreationError<DomainCompetence>>())
            return StatusCode(StatusCodes.Status500InternalServerError);

        return Ok(_mapper.Map<GetCompetenceResponse>(result.Value));
    }

    [HttpDelete]
    [SwaggerResponse(StatusCodes.Status200OK, "Competence deleted")]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Competence not found")]
    public async Task<IActionResult> DeleteCompetence(string id)
    {
        if (!Guid.TryParse(id, out var idGuid))
            return BadRequest();

        var result = await _competenceService.DeleteCompetenceAsync(idGuid);

        if (result.HasError<NotFoundError<DomainCompetence>>())
            return NotFound();

        return Ok();
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Competence updated")]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Competence not found")]
    public async Task<IActionResult> UpdateCompetence([FromBody] UpdateCompetenceRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _competenceService.UpdateCompetenceAsync(Guid.Parse(request.Id), request.NewName,
            request.NewDescription, _mapper.Map<DomainDifficultyLevel>(request.NewDifficultyLevel));

        if (result.HasError<NotFoundError<DomainCompetence>>())
            return NotFound();

        return Ok();
    }

    [HttpPost]
    [SwaggerResponse(StatusCodes.Status200OK, "Resource created", typeof(GetCompetenceResourceResponse))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Validation failed", typeof(ValidationFailedResponse))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Competence not found")]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, "Creation error")]
    public async Task<IActionResult> CreateCompetenceResource([FromBody] CreateCompetenceResourceRequest request)
    {
        var validationResult = await _validationService.ValidateAsync(request);
        if (validationResult.IsFailed)
            return BadRequest(new ValidationFailedResponse(validationResult.Errors.Select(x => x.Message)));

        var result = await _competenceService.CreateCompetenceResourceAsync(Guid.Parse(request.CompetenceId), request.Name, request.Url);
        if (result.HasError<NotFoundError<DomainCompetence>>())
            return NotFound();

        if (result.HasError<CreationError<DomainCompetenceResource>>())
            return StatusCode(StatusCodes.Status500InternalServerError);
        
        return Ok(_mapper.Map<GetCompetenceResourceResponse>(result.Value));
    }

    [HttpDelete]
    [SwaggerResponse(StatusCodes.Status200OK, "Resource deleted")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Resource not found")]
    public async Task<IActionResult> DeleteCompetenceResource(string id)
    {
        if (!Guid.TryParse(id, out var idGuid))
            return BadRequest();

        var result = await _competenceService.DeleteCompetenceResourceAsync(idGuid);
        if (result.HasError<NotFoundError<DomainCompetenceResource>>())
            return NotFound();

        return Ok();
    }

    [HttpDelete]
    [SwaggerResponse(StatusCodes.Status200OK, "Remarks deleted")]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid Id")]
    [SwaggerResponse(StatusCodes.Status404NotFound, "Remarks not found")]
    public async Task<IActionResult> DeleteCompetenceRemarks(string id)
    {
        if (!Guid.TryParse(id, out var guidId))
            return BadRequest();

        var result = await _competenceService.DeleteRemarksAsync(guidId);

        if (result.HasError<NotFoundError<DomainCompetenceRemarks>>())
            return NotFound();

        return Ok();
    }
}