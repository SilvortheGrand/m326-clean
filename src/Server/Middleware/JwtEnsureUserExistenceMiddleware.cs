﻿using System.Net;
using Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Policy;
using Microsoft.AspNetCore.Identity;

namespace Server.Middleware;

public class JwtEnsureUserExistenceMiddleware : IAuthorizationMiddlewareResultHandler
{
    private readonly AuthorizationMiddlewareResultHandler _defaultHandler = new();
    private readonly UserManager<ApplicationUser> _userManager;

    public JwtEnsureUserExistenceMiddleware(UserManager<ApplicationUser> userManager)
    {
        _userManager = userManager;
    }

    public async Task HandleAsync(RequestDelegate next, HttpContext context, AuthorizationPolicy policy,
        PolicyAuthorizationResult authorizeResult)
    {
        if (authorizeResult.Succeeded && await _userManager.GetUserAsync(context.User) is null)
        {
            context.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
            return;
        }

        await _defaultHandler.HandleAsync(next, context, policy, authorizeResult);
    }
}