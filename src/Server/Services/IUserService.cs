﻿using System.Security.Claims;
using Domain.Entities;
using FluentResults;

namespace Server.Services;

public interface IUserService
{
    Task<Result<User>> GetUserFromClaimsPrincipalAsync(ClaimsPrincipal claimsPrincipal);
}