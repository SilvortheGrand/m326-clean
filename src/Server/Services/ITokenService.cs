﻿using Database;

namespace Server.Services;

public interface ITokenService
{
    Task<string> CreateTokenAsync(ApplicationUser user);
    Task<string> GenerateRefreshToken(ApplicationUser user);
}