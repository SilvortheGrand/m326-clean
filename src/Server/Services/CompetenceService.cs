﻿using AutoMapper;
using Database;
using Database.Models;
using Domain.Entities;
using Domain.Entities.Competences;
using Domain.Errors;
using Domain.Errors.Competences;
using FluentResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using IdentityConstants = Shared.IdentityConstants;

namespace Server.Services;

public class CompetenceService : ICompetenceService
{
    private readonly ApplicationDbContext _dbContext;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IMapper _mapper;
    private readonly ILogger<CompetenceService> _logger;

    public CompetenceService(ApplicationDbContext dbContext, IMapper mapper, UserManager<ApplicationUser> userManager,
        ILogger<CompetenceService> logger)
    {
        _dbContext = dbContext;
        _mapper = mapper;
        _userManager = userManager;
        _logger = logger;
    }

    public async Task<Result<IEnumerable<DomainSubjectInfo>>> GetSubjectOverviewAsync()
    {
        var subjects = await _dbContext.Subjects.ToListAsync();

        var results = new List<DomainSubjectInfo>();

        foreach (var subject in subjects)
        {
            var areaCount = await _dbContext.CompetenceAreas.Where(x => x.Subject.Equals(subject)).CountAsync();
            var competenceCount = await _dbContext.Competences.Where(x => x.CompetenceArea.Subject.Equals(subject))
                .CountAsync();

            results.Add(new DomainSubjectInfo(subject.Id, subject.Name, areaCount, competenceCount));
        }

        return Result.Ok(results.AsEnumerable());
    }

    public async Task<Result<DomainSubject>> GetSubjectAsync(Guid subjectId)
    {
        var subject = await _dbContext.Subjects.FindAsync(subjectId);
        if (subject is null)
            return Result.Fail(new NotFoundError<DomainSubject>($"Could not find subject with id {subjectId}."));

        subject.CompetenceAreas = await _dbContext.CompetenceAreas.Where(x => x.Subject == subject).ToListAsync();

        return Result.Ok(_mapper.Map<DomainSubject>(subject));
    }

    public async Task<Result<DomainSubject>> CreateSubjectAsync(string name)
    {
        try
        {
            if (await _dbContext.Subjects.AnyAsync(x => x.Name.ToLower() == name.ToLower()))
                return Result.Fail(new DuplicateError<DomainSubject>($"Subject with name: {name} already exists."));

            var subject = new Subject
            {
                Name = name
            };

            var result = await _dbContext.Subjects.AddAsync(subject);
            await _dbContext.SaveChangesAsync();

            _logger.LogInformation("Subject {subject} created", subject.Name);
            return Result.Ok(_mapper.Map<DomainSubject>(result.Entity));
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Subject creation failed.");
            return Result.Fail(new CreationError<DomainSubject>(e));
        }
    }

    public async Task<Result> DeleteSubjectAsync(Guid id)
    {
        if (!await _dbContext.Subjects.AnyAsync(x => x.Id.Equals(id)))
            return Result.Fail(new NotFoundError<DomainSubject>($"Could not find subject with id {id}."));

        await _dbContext.Subjects.Where(x => x.Id.Equals(id)).ExecuteDeleteAsync();
        await _dbContext.SaveChangesAsync();

        return Result.Ok();
    }

    public async Task<Result<DomainSubject>> UpdateSubjectAsync(Guid id, string newName)
    {
        var dbSubject = await _dbContext.Subjects.FindAsync(id);
        if (dbSubject is null)
            return Result.Fail(new NotFoundError<DomainSubject>($"Could not find subject with id {id}."));

        dbSubject.Name = newName;
        await _dbContext.SaveChangesAsync();

        return Result.Ok(_mapper.Map<DomainSubject>(dbSubject));
    }

    public async Task<Result<DomainCompetenceArea>> GetAreaAsync(Guid areaId)
    {
        var area = await _dbContext.CompetenceAreas.FindAsync(areaId);
        if (area is null)
            return Result.Fail(new NotFoundError<DomainCompetenceArea>($"Could not find area with id {areaId}."));

        area.Competences = await _dbContext.Competences.Where(x => x.CompetenceArea == area).ToListAsync();

        return Result.Ok(_mapper.Map<DomainCompetenceArea>(area));
    }

    public async Task<Result<DomainCompetenceArea>> CreateAreaAsync(Guid subjectId, string name)
    {
        try
        {
            var dbSubject = await _dbContext.Subjects.FindAsync(subjectId);
            if (dbSubject is null)
                return Result.Fail(new NotFoundError<DomainSubject>($"Could not find subject with id {subjectId}."));

            if (await _dbContext.CompetenceAreas.AnyAsync(x =>
                    x.Subject.Id == subjectId && x.Name.ToLower() == name.ToLower()))
                return Result.Fail(
                    new DuplicateError<DomainCompetenceArea>(
                        $"Area with the name {name} in the subject with the id ${subjectId} already exists!"));

            var dbArea = new CompetenceArea
            {
                Name = name,
                Subject = dbSubject
            };

            var result = await _dbContext.CompetenceAreas.AddAsync(dbArea);
            await _dbContext.SaveChangesAsync();
            return Result.Ok(_mapper.Map<DomainCompetenceArea>(result.Entity));
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Area Creation failed.");
            return Result.Fail(new CreationError<DomainCompetenceArea>(e));
        }
    }

    public async Task<Result> DeleteAreaAsync(Guid id)
    {
        var dbArea = await _dbContext.CompetenceAreas.FindAsync(id);
        if (dbArea is null)
            return Result.Fail(new NotFoundError<DomainCompetenceArea>($"Could not find area with id {id}"));

        await _dbContext.CompetenceAreas.Where(x => x.Id.Equals(id)).ExecuteDeleteAsync();
        await _dbContext.SaveChangesAsync();

        return Result.Ok();
    }

    public async Task<Result<DomainCompetenceArea>> UpdateAreaAsync(Guid areaId, string newName)
    {
        var dbArea = await _dbContext.CompetenceAreas.FindAsync(areaId);
        if (dbArea is null)
            return Result.Fail(new NotFoundError<DomainCompetenceArea>($"Could not find area with id {areaId}"));

        dbArea.Name = newName;

        await _dbContext.SaveChangesAsync();
        return Result.Ok(_mapper.Map<DomainCompetenceArea>(dbArea));
    }

    public async Task<Result<DomainCompetence>> GetCompetenceAsync(Guid competenceId)
    {
        var competence = await _dbContext.Competences.FindAsync(competenceId);
        if (competence is null)
            return Result.Fail(
                new NotFoundError<DomainCompetence>($"Could not find competence with id {competenceId}"));

        competence.Remarks = await _dbContext.CompetenceRemarks.Where(x => x.Competence == competence).ToListAsync();
        competence.Resources =
            await _dbContext.CompetenceResources.Where(x => x.Competence == competence).ToListAsync();

        return Result.Ok(_mapper.Map<DomainCompetence>(competence));
    }

    public async Task<Result<DomainCompetence>> CreateCompetenceAsync(Guid areaId, string name, string description,
        DomainDifficultyLevel difficultyLevel)
    {
        var dbArea = await _dbContext.CompetenceAreas.FindAsync(areaId);
        if (dbArea is null)
            return Result.Fail(new NotFoundError<DomainCompetenceArea>($"Could not find area with id {areaId}"));

        if (await _dbContext.Competences.AnyAsync(x =>
                x.Name.ToLower() == name.ToLower()))
            return Result.Fail(new DuplicateError<DomainCompetence>($"Competence with name {name} already exists"));

        var dbCompetence = new Competence
        {
            Name = name,
            Description = description,
            DifficultyLevel = _mapper.Map<DifficultyLevel>(difficultyLevel),
            CompetenceArea = dbArea
        };

        try
        {
            var result = await _dbContext.Competences.AddAsync(dbCompetence);
            await _dbContext.SaveChangesAsync();

            return Result.Ok(_mapper.Map<DomainCompetence>(result.Entity));
        }
        catch (Exception e)
        {
            return Result.Fail(new CreationError<DomainCompetence>(e));
        }
    }

    public async Task<Result> DeleteCompetenceAsync(Guid id)
    {
        var dbCompetence = await _dbContext.Competences.FindAsync(id);
        if (dbCompetence is null)
            return Result.Fail(new NotFoundError<DomainCompetence>($"Could not find competence with id {id}"));

        await _dbContext.Competences.Where(x => x.Id.Equals(id)).ExecuteDeleteAsync();
        await _dbContext.SaveChangesAsync();

        return Result.Ok();
    }

    public async Task<Result<DomainCompetence>> UpdateCompetenceAsync(Guid competenceId, string newName,
        string newDescription, DomainDifficultyLevel newLevel)
    {
        var dbCompetence = await _dbContext.Competences.FindAsync(competenceId);
        if (dbCompetence is null)
            return Result.Fail(
                new NotFoundError<DomainCompetence>($"Could not find competence with id {competenceId}"));

        dbCompetence.Name = newName;
        dbCompetence.Description = newDescription;
        dbCompetence.DifficultyLevel = _mapper.Map<DifficultyLevel>(newLevel);

        await _dbContext.SaveChangesAsync();

        return Result.Ok(_mapper.Map<DomainCompetence>(dbCompetence));
    }

    public async Task<Result<DomainCompetenceResource>> GetResourceAsync(Guid resourceId)
    {
        var resource = await _dbContext.CompetenceResources.FindAsync(resourceId);
        if (resource is null)
            return Result.Fail(
                new NotFoundError<DomainCompetenceResource>($"Could not find resource with id {resourceId}."));

        return Result.Ok(_mapper.Map<DomainCompetenceResource>(resource));
    }

    public async Task<Result<DomainCompetenceResource>> CreateCompetenceResourceAsync(Guid competenceId, string name,
        string url)
    {
        var dbCompetence = await _dbContext.Competences.FindAsync(competenceId);
        if (dbCompetence is null)
            return Result.Fail(
                new NotFoundError<DomainCompetence>($"Could not find a competence with the id {competenceId}"));

        var dbResource = new CompetenceResource
        {
            Name = name,
            Url = url,
            Competence = dbCompetence
        };

        try
        {
            var result = await _dbContext.CompetenceResources.AddAsync(dbResource);
            await _dbContext.SaveChangesAsync();

            return Result.Ok(_mapper.Map<DomainCompetenceResource>(result.Entity));
        }
        catch (Exception e)
        {
            return Result.Fail(new CreationError<DomainCompetenceResource>(e));
        }
    }

    public async Task<Result> DeleteCompetenceResourceAsync(Guid id)
    {
        if (!await _dbContext.CompetenceResources.AnyAsync(x => x.Id.Equals(id)))
            return Result.Fail(new NotFoundError<DomainCompetenceResource>($"Could not find resource with id {id}."));

        await _dbContext.CompetenceResources.Where(x => x.Id.Equals(id)).ExecuteDeleteAsync();
        await _dbContext.SaveChangesAsync();

        return Result.Ok();
    }

    public async Task<Result<DomainCompetenceRemarks>> GetRemarksAsync(Guid remarksId)
    {
        var remark = await _dbContext.CompetenceRemarks.Include(x => x.Author)
            .SingleOrDefaultAsync(x => x.Id == remarksId);
        if (remark is null)
            return Result.Fail(
                new NotFoundError<DomainCompetenceRemarks>($"Could not find remark with id {remarksId}."));

        return Result.Ok(_mapper.Map<DomainCompetenceRemarks>(remark));
    }

    public async Task<Result<DomainCompetenceRemarks>> CreateRemarkAsync(Guid competenceId, string content,
        Guid authorId)
    {
        var dbCompetence = await _dbContext.Competences.FindAsync(competenceId);
        if (dbCompetence is null)
            return Result.Fail(
                new NotFoundError<DomainCompetence>($"Could not find competence with id {competenceId}."));

        var dbUser = await _dbContext.Users.FindAsync(authorId.ToString());
        if (dbUser is null)
            return Result.Fail(new NotFoundError<User>($"Could not find user with id {authorId}."));

        var dbRemark = new CompetenceRemarks
        {
            Content = content,
            Author = dbUser,
            Competence = dbCompetence
        };

        try
        {
            var result = await _dbContext.CompetenceRemarks.AddAsync(dbRemark);
            await _dbContext.SaveChangesAsync();

            return Result.Ok(_mapper.Map<DomainCompetenceRemarks>(result.Entity));
        }
        catch (Exception e)
        {
            return Result.Fail(new CreationError<DomainCompetenceRemarks>(e));
        }
    }

    public async Task<Result> DeleteRemarksAsync(Guid id)
    {
        if (!await _dbContext.CompetenceRemarks.AnyAsync(x => x.Id.Equals(id)))
            return Result.Fail(new NotFoundError<CompetenceRemarks>($"Could not find remarks with id {id}."));

        await _dbContext.CompetenceRemarks.Where(x => x.Id.Equals(id)).ExecuteDeleteAsync();
        await _dbContext.SaveChangesAsync();

        return Result.Ok();
    }

    public async Task<Result> DeleteRemarksAsync(Guid id, Guid userId)
    {
        var remarks = await _dbContext.CompetenceRemarks.Include(x => x.Author).SingleOrDefaultAsync(x => x.Id == id);
        if (remarks is null)
            return Result.Fail(new NotFoundError<DomainCompetenceRemarks>($"Could not find remarks with id {id}"));

        var user = await _dbContext.Users.FindAsync(userId.ToString());
        if(user is null)
            return Result.Fail(new UnauthorizedError("User not authorized to delete remarks."));
        
        if(!remarks.Author.Id.Equals(user.Id) && !(await _userManager.GetRolesAsync(user)).Contains(IdentityConstants.ManagementRole))
            return Result.Fail(new UnauthorizedError("User not authorized to delete remarks."));

        _dbContext.CompetenceRemarks.Remove(remarks);
        await _dbContext.SaveChangesAsync();

        return Result.Ok();
    }
}