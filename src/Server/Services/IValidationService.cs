﻿using FluentResults;

namespace Server.Services;

public interface IValidationService
{
    Task<Result> ValidateAsync<T>(T validateObject);
}