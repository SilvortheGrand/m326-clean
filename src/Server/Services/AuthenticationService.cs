﻿using System.Security.Claims;
using Database;
using Domain.Entities;
using Domain.Entities.Auth;
using Domain.Errors;
using Domain.Errors.Auth;
using FluentResults;
using Server.Auth;

namespace Server.Services;

public class AuthenticationService : IAuthenticationService
{
    private readonly ITokenService _tokenService;
    private readonly CustomUserManager<ApplicationUser> _userManager;

    public AuthenticationService(ITokenService tokenService, CustomUserManager<ApplicationUser> userManager)
    {
        _tokenService = tokenService;
        _userManager = userManager;
    }

    public async Task<Result<AuthSuccess>> LoginAsync(string username, string password)
    {
        var user = await _userManager.FindByNameAsync(username);
        if (user is null || !await _userManager.CheckPasswordAsync(user, password))
            return Result.Fail(new InvalidCredentialError());

        await _userManager.UpdateSecurityStampAsync(user);

        return Result.Ok(new AuthSuccess(await _tokenService.CreateTokenAsync(user),
            await _tokenService.GenerateRefreshToken(user)));
    }

    public async Task<Result<AuthSuccess>> RegisterAsync(string username, string email, string password,
        string passwordConfirm)
    {
        var user = await _userManager.FindByNameAsync(username);
        if (user is not null)
            return Result.Fail(new DuplicateUserError());

        user = new ApplicationUser
        {
            Email = email,
            UserName = username
        };

        var result = await _userManager.CreateAsync(user, password);
        if (!result.Succeeded)
            return Result.Fail(
                result.Errors.Select(x => new UserCreationError(x.Description)));

        return Result.Ok(new AuthSuccess(await _tokenService.CreateTokenAsync(user),
            await _tokenService.GenerateRefreshToken(user)));
    }

    public async Task LogoutAsync(ClaimsPrincipal principal)
    {
        var user = await _userManager.GetUserAsync(principal);
        await _userManager.RemoveAuthenticationTokenAsync(user!, AuthenticationDefaults.LoginProviderName,
            AuthenticationDefaults.RefreshTokenName);
    }

    public async Task<Result<AuthSuccess>> RefreshTokenAsync(string refreshToken)
    {
        var user = await _userManager.FindByAuthenticationTokenAsync(refreshToken);
        if (user is null || await _userManager.VerifyUserTokenAsync(user, AuthenticationDefaults.LoginProviderName,
                AuthenticationDefaults.RefreshTokenName, refreshToken) == false)
            return Result.Fail(new InvalidRefreshTokenError());

        return Result.Ok(new AuthSuccess(await _tokenService.CreateTokenAsync(user),
            await _tokenService.GenerateRefreshToken(user)));
    }

    public async Task<Result<User>> GetCurrentUserAsync(ClaimsPrincipal claimsPrincipal)
    {
        var user = await _userManager.GetUserAsync(claimsPrincipal);
        return Result.Ok(new User {Username = user!.UserName!, Email = user.Email!, Id = Guid.Parse(user.Id)});
    }
}