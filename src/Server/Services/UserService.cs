﻿using System.Security.Claims;
using AutoMapper;
using Database;
using Domain.Entities;
using Domain.Errors.Competences;
using FluentResults;
using Microsoft.AspNetCore.Identity;

namespace Server.Services;

public class UserService : IUserService
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IMapper _mapper;

    public UserService(UserManager<ApplicationUser> userManager, IMapper mapper)
    {
        _userManager = userManager;
        _mapper = mapper;
    }

    public async Task<Result<User>> GetUserFromClaimsPrincipalAsync(ClaimsPrincipal claimsPrincipal)
    {
        var user = await _userManager.GetUserAsync(claimsPrincipal);
        if (user is null)
            return Result.Fail(new NotFoundError<User>("User could not be found."));
        
        return Result.Ok(_mapper.Map<User>(user));
    }
}