﻿using System.Security.Claims;
using Domain.Entities;
using Domain.Entities.Auth;
using FluentResults;

namespace Server.Services;

public interface IAuthenticationService
{
    Task<Result<AuthSuccess>> LoginAsync(string username, string password);
    Task<Result<AuthSuccess>> RegisterAsync(string username, string email, string password, string passwordConfirm);
    Task LogoutAsync(ClaimsPrincipal principal);
    Task<Result<AuthSuccess>> RefreshTokenAsync(string refreshToken);
    Task<Result<User>> GetCurrentUserAsync(ClaimsPrincipal claimsPrincipal);
}