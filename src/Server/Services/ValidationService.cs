﻿using Client.Errors;
using FluentResults;
using FluentValidation;

namespace Server.Services;

public class ValidationService : IValidationService
{
    private readonly IServiceProvider _serviceProvider;

    public ValidationService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task<Result> ValidateAsync<T>(T validateObject)
    {
        var validators = _serviceProvider.GetService<IEnumerable<IValidator<T>>>()?.ToList();
        if (validators is null || !validators.Any())
            Result.Ok();

        var context = new ValidationContext<T>(validateObject);
        var validationResults = await Task.WhenAll(validators!.Select(x => x.ValidateAsync(context)));
        var failures = validationResults.SelectMany(x => x.Errors).Where(x => x is not null).ToList();
        
        return failures.Any()
            ? Result.Fail(new ValidationFailedError(failures.Select(x => x.ErrorMessage)))
            : Result.Ok();
    }
}