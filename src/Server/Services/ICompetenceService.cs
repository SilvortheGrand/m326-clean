﻿using Domain.Entities.Competences;
using FluentResults;

namespace Server.Services;

public interface ICompetenceService
{
    Task<Result<IEnumerable<DomainSubjectInfo>>> GetSubjectOverviewAsync();
    Task<Result<DomainSubject>> GetSubjectAsync(Guid subjectId);
    Task<Result<DomainSubject>> CreateSubjectAsync(string name);
    Task<Result> DeleteSubjectAsync(Guid id);
    Task<Result<DomainSubject>> UpdateSubjectAsync(Guid id, string newName);

    Task<Result<DomainCompetenceArea>> GetAreaAsync(Guid areaId);
    Task<Result<DomainCompetenceArea>> CreateAreaAsync(Guid subjectId, string name);
    Task<Result> DeleteAreaAsync(Guid id);
    Task<Result<DomainCompetenceArea>> UpdateAreaAsync(Guid areaId, string newName);

    Task<Result<DomainCompetence>> GetCompetenceAsync(Guid competenceId);
    Task<Result<DomainCompetence>> CreateCompetenceAsync(Guid areaId, string name, string description, DomainDifficultyLevel difficultyLevel);
    Task<Result> DeleteCompetenceAsync(Guid id);
    Task<Result<DomainCompetence>> UpdateCompetenceAsync(Guid competenceId, string newName, string newDescription, DomainDifficultyLevel newLevel);

    Task<Result<DomainCompetenceResource>> GetResourceAsync(Guid resourceId);
    Task<Result<DomainCompetenceResource>> CreateCompetenceResourceAsync(Guid competenceId, string name, string url);
    Task<Result> DeleteCompetenceResourceAsync(Guid id);

    Task<Result<DomainCompetenceRemarks>> GetRemarksAsync(Guid remarksId);
    Task<Result<DomainCompetenceRemarks>> CreateRemarkAsync(Guid competenceId, string content, Guid authorId);
    Task<Result> DeleteRemarksAsync(Guid id);
    Task<Result> DeleteRemarksAsync(Guid id, Guid userId);
}