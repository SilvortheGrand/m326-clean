﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Server.Auth;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace Server.Services;

public class TokenService : ITokenService
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly JwtConfiguration _jwtConfiguration;
    private readonly IHostEnvironment _hostEnvironment;

    public TokenService(UserManager<ApplicationUser> userManager, IOptions<JwtConfiguration> jwtConfiguration,
        IHostEnvironment hostEnvironment)
    {
        _userManager = userManager;
        _hostEnvironment = hostEnvironment;
        _jwtConfiguration = jwtConfiguration.Value;
    }

    public async Task<string> CreateTokenAsync(ApplicationUser user)
    {
        var signingCredentials = GenerateSigningCredentials();
        var claims = await GetClaimsAsync(user);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Audience = _jwtConfiguration.ValidAudience,
            Issuer = _jwtConfiguration.ValidIssuer,
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.AddMinutes(_hostEnvironment.IsDevelopment()
                ? 30
                : _jwtConfiguration.ExpireInMinutes),
            SigningCredentials = signingCredentials
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);

        return tokenHandler.WriteToken(token);
    }

    public async Task<string> GenerateRefreshToken(ApplicationUser user)
    {
        await _userManager.RemoveAuthenticationTokenAsync(user, AuthenticationDefaults.LoginProviderName,
            AuthenticationDefaults.RefreshTokenName);
        var newToken = await _userManager.GenerateUserTokenAsync(user, AuthenticationDefaults.LoginProviderName,
            AuthenticationDefaults.RefreshTokenName);
        await _userManager.SetAuthenticationTokenAsync(user, AuthenticationDefaults.LoginProviderName,
            AuthenticationDefaults.RefreshTokenName, newToken);

        return newToken;
    }

    private SigningCredentials GenerateSigningCredentials()
    {
        return new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfiguration.SecurityKey)),
            SecurityAlgorithms.HmacSha256);
    }

    private async Task<IEnumerable<Claim>> GetClaimsAsync(ApplicationUser user)
    {
        var claims = new List<Claim>
        {
            new(ClaimTypes.NameIdentifier, user.Id),
            new(JwtRegisteredClaimNames.Email, user.Email!),
            new(JwtRegisteredClaimNames.UniqueName, user.Id),
            new(JwtRegisteredClaimNames.Name, user.UserName!)
        };
        
        foreach(var role in await _userManager.GetRolesAsync(user))
            claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));

        return claims;
    }
}