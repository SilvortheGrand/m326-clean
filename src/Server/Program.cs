using System.Text.Json.Serialization;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using Server.Auth;
using Server.Middleware;
using Server.Services;
using Swashbuckle.AspNetCore.SwaggerGen.ConventionalRouting;

namespace Server;

internal class Program
{
    private static async Task Main(string[] args)
    {
        var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

        Log.Logger = new LoggerConfiguration().ReadFrom
            .Configuration(configuration)
            .CreateLogger();

        try
        {
            Log.ForContext<Program>().Information("Starting up...");

            var builder = CreateHostBuilder(args);

            var app = ConfigureApp(builder.Build());

            await app.RunAsync();
        }
        catch (Exception e)
        {
            Log.ForContext<Program>().Fatal(e, "Startup failed");
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }

    private static WebApplicationBuilder CreateHostBuilder(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Logging.ClearProviders();
        builder.Logging.AddSerilog(Log.Logger);

        builder.Services.AddTransient<IValidationService, ValidationService>();
        builder.Services.AddValidatorsFromAssembly(typeof(Program).Assembly);
        builder.Services.AddAutoMapper(typeof(Program).Assembly);

        builder.Services.AddMyAuthentication(builder.Configuration);

        builder.Services.AddTransient<IUserService, UserService>();
        builder.Services.AddTransient<ICompetenceService, CompetenceService>();

        builder.Services.AddControllers()
            .AddJsonOptions(o => o.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        builder.Services.AddSwaggerGenWithConventionalRoutes();

        return builder;
    }

    private static WebApplication ConfigureApp(WebApplication app)
    {
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
            app.UseDeveloperExceptionPage();
            app.UseWebAssemblyDebugging();
        }

        app.UseHttpsRedirection();
        app.UseBlazorFrameworkFiles();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.MapControllerRoute("default", "api/{controller}/{action}/{id?}");
        app.MapFallbackToFile("index.html");
        ConventionalRoutingSwaggerGen.UseRoutes(app);

        return app;
    }
}