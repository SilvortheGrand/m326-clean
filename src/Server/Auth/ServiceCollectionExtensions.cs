﻿using Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Server.Middleware;
using Server.Services;
using IdentityConstants = Shared.IdentityConstants;

namespace Server.Auth;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddMyAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<JwtConfiguration>(configuration.GetSection("JWTSettings"));

        services.AddDatabase(configuration);

        services.AddTransient<ITokenService, TokenService>();
        services.AddTransient<IAuthenticationService, AuthenticationService>();

        services.AddIdentity<ApplicationUser, IdentityRole>()
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddUserManager<CustomUserManager<ApplicationUser>>()
            .AddDefaultTokenProviders()
            .AddTokenProvider(AuthenticationDefaults.LoginProviderName,
                typeof(DataProtectorTokenProvider<ApplicationUser>));

        services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer();

        services.AddAuthorization(o =>
        {
            o.AddPolicy(AuthenticationDefaults.ManagementPolicyName, b => b.RequireAuthenticatedUser()
                .RequireRole(IdentityConstants.ManagementRole));
            o.AddPolicy(AuthenticationDefaults.TeacherPolicyName, b => b.RequireAuthenticatedUser()
                .RequireRole(IdentityConstants.TeacherRole, IdentityConstants.ManagementRole));
        });

        services.ConfigureOptions<CustomJwtOptions>();

        services.AddScoped<IAuthorizationMiddlewareResultHandler, JwtEnsureUserExistenceMiddleware>();

        return services;
    }
}