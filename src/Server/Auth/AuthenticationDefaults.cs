﻿namespace Server.Auth;

public static class AuthenticationDefaults
{
    public const string LoginProviderName = "M326";
    public const string RefreshTokenName = "RefreshToken";
    public const string ManagementPolicyName = "Management";
    public const string TeacherPolicyName = "Teacher";
}