﻿using Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Server.Auth;

public sealed class CustomUserManager<TUser> : UserManager<TUser> where TUser : IdentityUser
{
    private readonly ApplicationDbContext _dbContext;

    public CustomUserManager(IUserStore<TUser> store, IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<TUser> passwordHasher, IEnumerable<IUserValidator<TUser>> userValidators,
        IEnumerable<IPasswordValidator<TUser>> passwordValidators, ILookupNormalizer keyNormalizer,
        IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<TUser>> logger,
        ApplicationDbContext dbContext) : base(store,
        optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
    {
        _dbContext = dbContext;
    }

    public async Task<TUser?> FindByAuthenticationTokenAsync(string tokenString,
        CancellationToken cancellationToken = default)
    {
        var token = await _dbContext.UserTokens.FirstOrDefaultAsync(x => x.Value != null && x.Value.Equals(tokenString),
            cancellationToken);
        
        if (token is null)
            return null;

        return await FindByIdAsync(token.UserId);
    }
}