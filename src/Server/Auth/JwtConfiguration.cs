﻿namespace Server.Auth;

public class JwtConfiguration
{
    public string SecurityKey { get; set; } = null!;
    public string ValidIssuer { get; set; } = null!;
    public string ValidAudience { get; set; } = null!;
    public int ExpireInMinutes { get; set; }
}