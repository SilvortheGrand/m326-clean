﻿using System.Diagnostics;
using AutoMapper;
using Client.Models;
using Client.Models.Competences;
using Shared;
using Shared.Competences;

namespace Client.Mappings;

public class ResponseMappings : Profile
{
    public ResponseMappings()
    {
        CreateMap<GetSubjectOverviewResponse, SubjectSummary>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.AreaCount, o => o.MapFrom(src => src.AreaCount))
            .ForMember(dest => dest.CompetenceCount, o => o.MapFrom(src => src.CompetenceCount));
        
        CreateMap<GetSubjectResponse, Subject>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Areas, o => o.MapFrom(src => src.Areas));

        CreateMap<GetCompetenceAreaResponse, CompetenceArea>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Competences, o => o.MapFrom(src => src.Competences));

        CreateMap<GetCompetenceResponse, Competence>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Description, o => o.MapFrom(src => src.Description))
            .ForMember(dest => dest.DifficultyLevel, o => o.MapFrom(src => src.DifficultyLevel))
            .ForMember(dest => dest.Resources, o => o.MapFrom(src => src.Resources))
            .ForMember(dest => dest.Remarks, o => o.MapFrom(src => src.Remarks));

        CreateMap<GetCompetenceResourceResponse, CompetenceResource>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, o => o.MapFrom(src => src.Name))
            .ForMember(dest => dest.Url, o => o.MapFrom(src => src.Url));

        CreateMap<GetCompetenceRemarksResponse, CompetenceRemarks>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Content, o => o.MapFrom(src => src.Content))
            .ForMember(dest => dest.Author, o => o.MapFrom(src => src.Author));

        CreateMap<UserResponse, User>()
            .ForMember(dest => dest.Id, o => o.MapFrom(src => src.Id))
            .ForMember(dest => dest.Username, o => o.MapFrom(src => src.Username));
    }
}