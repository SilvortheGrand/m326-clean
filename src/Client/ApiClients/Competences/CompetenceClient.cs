﻿using System.Net;
using AutoMapper;
using Client.Errors;
using Client.Helpers;
using Client.Models.Competences;
using FluentResults;
using Shared.Competences;

namespace Client.ApiClients.Competences;

public class CompetenceClient : ICompetenceClient
{
    private readonly HttpClient _httpClient;
    private readonly IMapper _mapper;

    public CompetenceClient(HttpClient httpClient, IMapper mapper)
    {
        _httpClient = httpClient;
        _mapper = mapper;
    }

    public async Task<Result<Subject>> GetSubjectAsync(string id)
    {
        var response = await _httpClient.GetAsync($"api/Competence/GetSubject/{id}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, $"Could not find subject with id {id}."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetSubjectResponse>();
                return Result.Ok(_mapper.Map<Subject>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<CompetenceArea>> GetAreaAsync(string id)
    {
        var response = await _httpClient.GetAsync($"api/Competence/GetArea/{id}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, $"Could not find area with id {id}."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceAreaResponse>();
                return Result.Ok(_mapper.Map<CompetenceArea>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<Competence>> GetCompetenceAsync(string id)
    {
        var response = await _httpClient.GetAsync($"api/Competence/GetCompetence/{id}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, $"Could not find competence with id {id}."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceResponse>();
                return Result.Ok(_mapper.Map<Competence>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<CompetenceRemarks>> GetRemarksAsync(string id)
    {
        var response = await _httpClient.GetAsync($"api/Competence/GetRemarks/{id}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, $"Could not find remarks with id {id}."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceRemarksResponse>();
                return Result.Ok(_mapper.Map<CompetenceRemarks>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result> DeleteRemarkAsync(string id)
    {
        var response = await _httpClient.DeleteAsync($"api/Competence/DeleteRemarks/{id}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, "Remarks not found."));
            case HttpStatusCode.Unauthorized:
                return Result.Fail(new UnauthorizedError("Unauthorized to delete these remarks."));
            case HttpStatusCode.OK:
                return Result.Ok();
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<CompetenceResource>> GetResourceAsync(string id)
    {
        var response = await _httpClient.GetAsync($"api/Competence/GetResource/{id}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, $"Could not find resource with id {id}."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceResourceResponse>();
                return Result.Ok(_mapper.Map<CompetenceResource>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<IEnumerable<SubjectSummary>>> GetSubjectOverviewAsync()
    {
        var response = await _httpClient.GetAsync("api/Competence/GetSubjectOverview");

        switch (response.StatusCode)
        {
            case HttpStatusCode.Unauthorized:
                return Result.Fail(new UnauthorizedError("User not authorized."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<IEnumerable<GetSubjectOverviewResponse>>();
                return Result.Ok(_mapper.Map<IEnumerable<SubjectSummary>>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }
}