﻿using Client.Models.Competences;
using FluentResults;

namespace Client.ApiClients.Competences;

public interface ICompetenceClient
{
    Task<Result<Subject>> GetSubjectAsync(string id);
    Task<Result<CompetenceArea>> GetAreaAsync(string id);
    Task<Result<Competence>> GetCompetenceAsync(string id);
    Task<Result<CompetenceRemarks>> GetRemarksAsync(string id);
    Task<Result> DeleteRemarkAsync(string id);
    Task<Result<CompetenceResource>> GetResourceAsync(string id);
    Task<Result<IEnumerable<SubjectSummary>>> GetSubjectOverviewAsync();
}