﻿using Client.Models.Competences;
using FluentResults;

namespace Client.ApiClients.Competences;

public interface ICompetenceManagementClient
{
    Task<Result<Subject>> CreateSubjectAsync(string name);
    Task<Result<Subject>> UpdateSubjectAsync(string id, string newName);
    Task<Result> DeleteSubjectAsync(string id);

    Task<Result<CompetenceArea>> CreateAreaAsync(string subjectId, string name);
    Task<Result<CompetenceArea>> UpdateAreaAsync(string areaId, string name);
    Task<Result> DeleteAreaAsync(string id);

    Task<Result<Competence>> CreateCompetenceAsync(string areaId, string name, string description,
        DifficultyLevel difficultyLevel);
    Task<Result<Competence>> UpdateCompetenceAsync(string competenceId, string newName, string newDescription,
        DifficultyLevel newDifficultyLevel);
    Task<Result> DeleteCompetenceAsync(string competenceId);

    Task<Result<CompetenceResource>> CreateResourceAsync(string competenceId, string name, string url);
    Task<Result> DeleteResourceAsync(string resourceId);

    Task<Result<CompetenceRemarks>> CreateRemarksAsync(string competenceId, string content);
    Task<Result> DeleteRemarksAsync(string remarksId);
}