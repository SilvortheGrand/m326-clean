﻿using System.Net;
using AutoMapper;
using Client.Errors;
using Client.Helpers;
using Client.Models.Competences;
using FluentResults;
using Shared;
using Shared.Competences;

namespace Client.ApiClients.Competences;

public class CompetenceManagementClient : ICompetenceManagementClient
{
    private readonly HttpClient _httpClient;
    private readonly IMapper _mapper;

    public CompetenceManagementClient(HttpClient httpClient, IMapper mapper)
    {
        _httpClient = httpClient;
        _mapper = mapper;
    }

    public async Task<Result<Subject>> CreateSubjectAsync(string name)
    {
        var body = HttpHelpers.CreateStringContent(new CreateSubjectRequest(name));
        var response = await _httpClient.PostAsync("api/CompetenceManagement/CreateSubject", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.Conflict:
                return Result.Fail(new DuplicateEntryError(name, "Subject with this name already exists."));
            case HttpStatusCode.InternalServerError:
                return Result.Fail(new CreationError("Could not create subject."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetSubjectResponse>();
                return Result.Ok(_mapper.Map<Subject>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<Subject>> UpdateSubjectAsync(string id, string newName)
    {
        var body = HttpHelpers.CreateStringContent(new UpdateSubjectRequest(id, newName));
        var response = await _httpClient.PostAsync("api/CompetenceManagement/UpdateSubject", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.NotFound:
                return Result.Fail(new CreationError("Could not find subject."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetSubjectResponse>();
                return Result.Ok(_mapper.Map<Subject>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result> DeleteSubjectAsync(string id)
    {
        var response = await _httpClient.DeleteAsync($"api/CompetenceManagement/DeleteSubject/{id}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid subject id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, "Could not find subject."));
            case HttpStatusCode.OK:
                return Result.Ok();
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<CompetenceArea>> CreateAreaAsync(string subjectId, string name)
    {
        var body = HttpHelpers.CreateStringContent(new CreateCompetenceAreaRequest(subjectId, name));
        var response = await _httpClient.PostAsync($"api/CompetenceManagement/CreateCompetenceArea/", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(subjectId, "Could not find subject."));
            case HttpStatusCode.Conflict:
                return Result.Fail(new DuplicateEntryError(name, "Area with this name already exists."));
            case HttpStatusCode.InternalServerError:
                return Result.Fail(new CreationError("Could not create area."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceAreaResponse>();
                return Result.Ok(_mapper.Map<CompetenceArea>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<CompetenceArea>> UpdateAreaAsync(string areaId, string name)
    {
        var body = HttpHelpers.CreateStringContent(new UpdateSubjectRequest(areaId, name));
        var response = await _httpClient.PostAsync("api/CompetenceManagement/UpdateCompetenceArea", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(areaId, "Could not find area."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceAreaResponse>();
                return Result.Ok(_mapper.Map<CompetenceArea>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result> DeleteAreaAsync(string id)
    {
        var response = await _httpClient.DeleteAsync($"api/CompetenceManagement/DeleteCompetenceArea/{id}");
        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid Id" }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(id, "Area not found."));
            case HttpStatusCode.OK:
                return Result.Ok();
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode));
        }
    }

    public async Task<Result<Competence>> CreateCompetenceAsync(string areaId, string name, string description,
        DifficultyLevel difficultyLevel)
    {
        var body = HttpHelpers.CreateStringContent(new CreateCompetenceRequest(areaId, name, description,
            difficultyLevel switch
            {
                DifficultyLevel.Easy => SharedDifficultyLevel.Easy,
                DifficultyLevel.Hard => SharedDifficultyLevel.Hard,
                DifficultyLevel.Medium => SharedDifficultyLevel.Medium,
                _ => throw new ArgumentOutOfRangeException(nameof(difficultyLevel), difficultyLevel, null)
            }));

        var response = await _httpClient.PostAsync("api/CompetenceManagement/CreateCompetence", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(areaId, "Could not find area."));
            case HttpStatusCode.Conflict:
                return Result.Fail(new DuplicateEntryError(name, "Competence already exists."));
            case HttpStatusCode.InternalServerError:
                return Result.Fail(new CreationError("Could not create competence."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceResponse>();
                return Result.Ok(_mapper.Map<Competence>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<Competence>> UpdateCompetenceAsync(string competenceId, string newName,
        string newDescription,
        DifficultyLevel newDifficultyLevel)
    {
        var body = HttpHelpers.CreateStringContent(new UpdateCompetenceRequest(competenceId, newName, newDescription,
            newDifficultyLevel switch
            {
                DifficultyLevel.Easy => SharedDifficultyLevel.Easy,
                DifficultyLevel.Medium => SharedDifficultyLevel.Medium,
                DifficultyLevel.Hard => SharedDifficultyLevel.Hard,
                _ => throw new ArgumentOutOfRangeException(nameof(newDifficultyLevel), newDifficultyLevel, null)
            }));
        var response = await _httpClient.PostAsync("api/CompetenceManagement/UpdateCompetence", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(competenceId, "Competence not found."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceResponse>();
                return Result.Ok(_mapper.Map<Competence>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result> DeleteCompetenceAsync(string competenceId)
    {
        var response = await _httpClient.DeleteAsync($"api/CompetenceManagement/DeleteCompetence/{competenceId}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid id" }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(competenceId, "Competence not found."));
            case HttpStatusCode.OK:
                return Result.Ok();
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<CompetenceResource>> CreateResourceAsync(string competenceId, string name, string url)
    {
        var body = HttpHelpers.CreateStringContent(new CreateCompetenceResourceRequest(competenceId, name, url));
        var response = await _httpClient.PostAsync("api/CompetenceManagement/CreateCompetenceResource", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(competenceId, "Competence not found."));
            case HttpStatusCode.InternalServerError:
                return Result.Fail(new CreationError("Could not create competence."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceResourceResponse>();
                return Result.Ok(new CompetenceResource(content!.Id, content.Name, content.Url));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result> DeleteResourceAsync(string resourceId)
    {
        var response = await _httpClient.DeleteAsync($"api/CompetenceManagement/DeleteCompetenceResource/{resourceId}");

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid Id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(resourceId, "Resource not found."));
            case HttpStatusCode.OK:
                return Result.Ok();
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result<CompetenceRemarks>> CreateRemarksAsync(string competenceId, string remark)
    {
        var body = HttpHelpers.CreateStringContent(new CreateCompetenceRemarksRequest(competenceId, remark));
        var response = await _httpClient.PostAsync("api/Competence/CreateCompetenceRemarks", body);

        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.Unauthorized:
                return Result.Fail(new UnauthorizedError("User could not be found."));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(competenceId, "Could not find competence."));
            case HttpStatusCode.InternalServerError:
                return Result.Fail(new CreationError("Could not create remark."));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCompetenceRemarksResponse>();
                return Result.Ok(_mapper.Map<CompetenceRemarks>(content));
            }
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }

    public async Task<Result> DeleteRemarksAsync(string remarksId)
    {
        var response = await _httpClient.DeleteAsync($"api/CompetenceManagement/DeleteCompetenceRemarks/{remarksId}");
        switch (response.StatusCode)
        {
            case HttpStatusCode.BadRequest:
                return Result.Fail(new ValidationFailedError(new[] { "Invalid Id." }));
            case HttpStatusCode.NotFound:
                return Result.Fail(new NotFoundError(remarksId, "Could not find remark."));
            case HttpStatusCode.OK:
                return Result.Ok();
            default:
                throw new ArgumentOutOfRangeException(nameof(response.StatusCode), response.StatusCode, "");
        }
    }
}