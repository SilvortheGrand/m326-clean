﻿using System.ComponentModel;
using System.Net;
using Client.Errors;
using Client.Helpers;
using FluentResults;
using Shared;
using Shared.Auth;

namespace Client.ApiClients;

public class AuthenticationClient : IAuthenticationClient
{
    private readonly HttpClient _httpClient;

    public AuthenticationClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<Result<AuthenticationResult>> LoginAsync(string username, string password)
    {
        var body = new LoginRequest(username, password);
        var response = await _httpClient.PostAsync("api/Auth/Login", HttpHelpers.CreateStringContent(body));

        if (response.StatusCode is HttpStatusCode.Unauthorized)
            return Result.Fail(new UnauthorizedError(await response.Content.ReadAsStringAsync()));

        var content = await response.DeserializeContentAsync<AuthSuccessResponse>();

        return Result.Ok(new AuthenticationResult(content!.Token));
    }

    public async Task<Result<AuthenticationResult>> RegisterAsync(string username, string email, string password,
        string passwordConfirm)
    {
        var body = new RegisterRequest(username, email, password, passwordConfirm);
        var response = await _httpClient.PostAsync("api/Auth/Register", HttpHelpers.CreateStringContent(body));

        switch (response.StatusCode)
        {
            case HttpStatusCode.Conflict:
                return Result.Fail(new DuplicateUserError());
            case HttpStatusCode.BadRequest:
            {
                var content = await response.DeserializeContentAsync<ValidationFailedResponse>();
                return Result.Fail(new ValidationFailedError(content!.Errors));
            }
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<AuthSuccessResponse>();
                return Result.Ok(new AuthenticationResult(content!.Token));
            }
            default:
                throw new InvalidEnumArgumentException(nameof(response.StatusCode));
        }
    }

    public async Task LogoutAsync()
    {
        await _httpClient.PostAsync("api/Auth/Logout", null);
    }

    public async Task<Result<AuthenticationResult>> RefreshTokenAsync()
    {
        var response = await _httpClient.PostAsync("api/Auth/RefreshToken", null);
        switch (response.StatusCode)
        {
            case HttpStatusCode.Unauthorized:
                return Result.Fail(new UnauthorizedError(await response.Content.ReadAsStringAsync()));
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<AuthSuccessResponse>();
                return Result.Ok(new AuthenticationResult(content!.Token));
            }
            default:
                throw new InvalidEnumArgumentException(nameof(response.StatusCode));
        }
    }

    public async Task<Result<CurrentUserResult>> GetCurrentUserAsync()
    {
        var response = await _httpClient.GetAsync($"api/Auth/GetCurrentUser");
        var failedResult = Result.Fail(new UnauthorizedError(await response.Content.ReadAsStringAsync()));
        
        switch (response.StatusCode)
        {
            case HttpStatusCode.Unauthorized:
                return failedResult;
            case HttpStatusCode.BadRequest:
                return failedResult;
            case HttpStatusCode.OK:
            {
                var content = await response.DeserializeContentAsync<GetCurrentUserResponse>();
                return Result.Ok(new CurrentUserResult(content!.Id, content.Username));
            }
            default:
                throw new InvalidEnumArgumentException(nameof(response.StatusCode));
        }
    }
}