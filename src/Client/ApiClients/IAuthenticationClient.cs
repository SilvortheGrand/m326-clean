﻿using Client.Authentication;
using FluentResults;

namespace Client.ApiClients;

public interface IAuthenticationClient
{
    Task<Result<AuthenticationResult>> LoginAsync(string username, string password);
    Task<Result<AuthenticationResult>> RegisterAsync(string username, string email, string password, string passwordConfirm);
    Task LogoutAsync();
    Task<Result<AuthenticationResult>> RefreshTokenAsync();
    Task<Result<CurrentUserResult>> GetCurrentUserAsync();
}