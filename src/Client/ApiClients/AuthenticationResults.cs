﻿namespace Client.ApiClients;

public record AuthenticationResult(string Token);
public record CurrentUserResult(string Id, string Username);