﻿using Client.Authentication;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace Client.Shared;

public partial class MainLayout
{
    [Inject] public CurrentUserService CurrentUserService { get; set; } = null!;
    [Inject] public AuthenticationStateProvider AuthenticationStateProvider { get; set; } = null!;

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        var state = await AuthenticationStateProvider.GetAuthenticationStateAsync();
        if (state.User.Identity?.IsAuthenticated == true)
            await CurrentUserService.PopulateUserAsync();
    }
}