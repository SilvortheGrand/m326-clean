﻿using Client.Authentication;
using Microsoft.AspNetCore.Components;

namespace Client.Shared;

public partial class NavMenu
{
    private bool _collapseNavMenu = true;

    private string? NavMenuCssClass => _collapseNavMenu ? "collapse" : null;

    [Inject] public CurrentUserService CurrentUserService { get; set; } = null!;

    protected override void OnInitialized()
    {
        CurrentUserService.PropertyChanged += (_, _) => StateHasChanged();
    }

    private void ToggleNavMenu()
    {
        _collapseNavMenu = !_collapseNavMenu;
    }
}