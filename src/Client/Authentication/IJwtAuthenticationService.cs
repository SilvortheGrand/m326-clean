﻿using Client.Models.Authentication;
using FluentResults;

namespace Client.Authentication;

public interface IJwtAuthenticationService
{
    event EventHandler<JwtAuthenticatedArgs> OnUserAuthenticationChanged;
    Task<Result<AuthenticationDetails>> LoginAsync(LoginModel model);
    Task<Result<AuthenticationDetails>> RegisterAsync(RegisterModel model);
    Task LogoutAsync();
    Task<Result<AuthenticationDetails>> RefreshTokenAsync();
    Task<Result<CurrentUserModel>> GetCurrentUserAsync();
}