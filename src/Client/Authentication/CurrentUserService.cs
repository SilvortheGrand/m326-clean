﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Client.Annotations;
using Client.Models.Authentication;

namespace Client.Authentication;

public class CurrentUserService : INotifyPropertyChanged
{
    private readonly IJwtAuthenticationService _jwtAuthenticationService;

    public CurrentUserService(IJwtAuthenticationService jwtAuthenticationService)
    {
        _jwtAuthenticationService = jwtAuthenticationService;
        _jwtAuthenticationService.OnUserAuthenticationChanged += JwtAuthenticationServiceOnOnUserAuthenticationChanged;
    }

    public CurrentUserModel CurrentUser { get; set; } = new(string.Empty, string.Empty);
    
    private async void JwtAuthenticationServiceOnOnUserAuthenticationChanged(object? sender, JwtAuthenticatedArgs e)
    {
        if (!string.IsNullOrEmpty(e.UserId))
            await PopulateUserAsync();
        else
            ClearUser();
    }

    private void ClearUser()
    {
        CurrentUser = new CurrentUserModel(string.Empty, string.Empty);
        OnPropertyChanged(nameof(CurrentUser));
    }

    public async Task PopulateUserAsync()
    {
        var result = await _jwtAuthenticationService.GetCurrentUserAsync();
        if (result.IsSuccess)
        {
            CurrentUser = result.Value;
            OnPropertyChanged(nameof(CurrentUser));
        }
        else
            ClearUser();
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}