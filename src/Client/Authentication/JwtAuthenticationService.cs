﻿using System.Security.Claims;
using Client.ApiClients;
using Client.Errors;
using Client.Models.Authentication;
using FluentResults;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.IdentityModel.JsonWebTokens;

namespace Client.Authentication;

public class JwtAuthenticationService : IJwtAuthenticationService
{
    private readonly AuthenticationStateProvider _authenticationStateProvider;
    private readonly RefreshTokenScheduler _refreshTokenScheduler;
    private readonly IAuthenticationClient _authenticationClient;

    public JwtAuthenticationService(AuthenticationStateProvider authenticationStateProvider,
        RefreshTokenScheduler refreshTokenScheduler, IAuthenticationClient authenticationClient)
    {
        _authenticationStateProvider = authenticationStateProvider;
        _refreshTokenScheduler = refreshTokenScheduler;
        _authenticationClient = authenticationClient;

        _authenticationStateProvider.AuthenticationStateChanged +=
            AuthenticationStateProviderOnAuthenticationStateChanged;
        _refreshTokenScheduler.OnRefresh += async () => await RefreshTokenAsync();
    }

    private async void AuthenticationStateProviderOnAuthenticationStateChanged(Task<AuthenticationState> task)
    {
        var result = await task;
        if (result.User.Identity is null || !result.User.Identity.IsAuthenticated)
            OnUserAuthenticationChanged?.Invoke(this, new JwtAuthenticatedArgs(string.Empty, string.Empty));
        else
        {
            var claimsIdentity = (ClaimsIdentity) result.User.Identity;
            var userId = claimsIdentity.FindFirst(JwtRegisteredClaimNames.UniqueName)!.Value;
            var username = claimsIdentity.FindFirst(JwtRegisteredClaimNames.Name)!.Value;
            OnUserAuthenticationChanged?.Invoke(this, new JwtAuthenticatedArgs(userId, username));
        }
    }

    public event EventHandler<JwtAuthenticatedArgs>? OnUserAuthenticationChanged;

    public async Task<Result<AuthenticationDetails>> LoginAsync(LoginModel model)
    {
        var result = await _authenticationClient.LoginAsync(model.Username, model.Password);
        if (result.HasError<UnauthorizedError>())
        {
            await LocalLogoutAsync();
            return Result.Fail(result.Errors);
        }

        var details = new AuthenticationDetails(string.Empty, result.Value.Token, DateTime.Now);
        await ((IJwtAuthenticationStateProvider) _authenticationStateProvider).MarkUserAsAuthenticatedAsync(details);

        _refreshTokenScheduler.Start();
        return Result.Ok(details);
    }

    public async Task<Result<AuthenticationDetails>> RegisterAsync(RegisterModel model)
    {
        var result = await _authenticationClient.RegisterAsync(model.Username, model.EmailAddress, model.Password,
            model.PasswordConfirm);
        if (result.IsFailed)
            return Result.Fail(result.Errors);

        var details = new AuthenticationDetails(string.Empty, result.Value.Token, DateTime.Now);
        await ((IJwtAuthenticationStateProvider) _authenticationStateProvider).MarkUserAsAuthenticatedAsync(details);
        _refreshTokenScheduler.Start();

        return Result.Ok(details);
    }

    public async Task LogoutAsync()
    {
        await _authenticationClient.LogoutAsync();
        await LocalLogoutAsync();
    }

    public async Task<Result<AuthenticationDetails>> RefreshTokenAsync()
    {
        var result = await _authenticationClient.RefreshTokenAsync();
        if (result.IsFailed)
        {
            await LocalLogoutAsync();
            return Result.Fail(result.Errors);
        }

        var details = new AuthenticationDetails(string.Empty, result.Value.Token, DateTime.Now);
        await ((IJwtAuthenticationStateProvider) _authenticationStateProvider).MarkUserAsLoggedOutAsync();
        return details;
    }

    public async Task<Result<CurrentUserModel>> GetCurrentUserAsync()
    {
        var result = await _authenticationClient.GetCurrentUserAsync();
        if (result.IsFailed)
        {
            await LocalLogoutAsync();
            return Result.Fail(result.Errors);
        }

        return Result.Ok(new CurrentUserModel(result.Value.Id, result.Value.Username));
    }

    private async Task LocalLogoutAsync()
    {
        await ((IJwtAuthenticationStateProvider) _authenticationStateProvider).MarkUserAsLoggedOutAsync();
        _refreshTokenScheduler.Stop();
    }
}