﻿namespace Client.Authentication;

public record JwtAuthenticatedArgs(string UserId, string Username);