﻿using System.Security.Claims;

namespace Client.Authentication;

public record AuthenticationDetails(string UserId, string Token, DateTime ExpirationDate);
public record CurrentUser(string UserId, string Username);
public record SavedToken(IEnumerable<Claim> Claims, AuthenticationDetails AuthenticationDetails);