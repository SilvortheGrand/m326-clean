﻿namespace Client.Authentication;

public interface IJwtAuthenticationStateProvider
{
    Task MarkUserAsAuthenticatedAsync(AuthenticationDetails details);
    Task MarkUserAsLoggedOutAsync();
}