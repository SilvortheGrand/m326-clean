﻿using System.Timers;
using FluentResults;
using Timer = System.Timers.Timer;

namespace Client.Authentication;

public class RefreshTokenScheduler
{
    private readonly Timer _timer;

    public event Func<Task<Result<AuthenticationDetails>>>? OnRefresh; 

    public RefreshTokenScheduler()
    {
        _timer = new Timer(TimeSpan.FromMinutes(2).TotalMilliseconds);
        _timer.Elapsed += TimerOnElapsed;
    }

    public void Start() => _timer.Start();
    public void Stop() => _timer.Stop();
    
    private void TimerOnElapsed(object? sender, ElapsedEventArgs e)
    {
        OnRefresh?.Invoke();
    }
}