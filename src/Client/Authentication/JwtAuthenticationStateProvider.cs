﻿using System.Net.Http.Headers;
using System.Security.Claims;
using Blazored.LocalStorage;
using Client.Helpers;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.IdentityModel.JsonWebTokens;
using Shared;

namespace Client.Authentication;

public class JwtAuthenticationStateProvider : AuthenticationStateProvider, IJwtAuthenticationStateProvider
{
    private const string TokenKey = "token";
    private const string ExpirationKey = "expiration";

    private static readonly AuthenticationState Anonymous = new(new ClaimsPrincipal(new ClaimsIdentity()));

    private readonly ILocalStorageService _localStorageService;
    private readonly HttpClient _httpClient;

    public JwtAuthenticationStateProvider(ILocalStorageService localStorageService, HttpClient httpClient)
    {
        _localStorageService = localStorageService;
        _httpClient = httpClient;
    }

    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        var token = await _localStorageService.GetItemAsync<string>(TokenKey);

        if (string.IsNullOrEmpty(token))
            return Anonymous;

        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);

        var principal = new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJwt(token), TokenKey, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType));
        var claims = principal.Claims.ToList();
        
        return new AuthenticationState(principal);
    }

    public async Task MarkUserAsAuthenticatedAsync(AuthenticationDetails details)
    {
        var token = ParseToken(details);
        await MarkUserAsAuthenticatedAsync(token);
    }

    private async Task MarkUserAsAuthenticatedAsync(SavedToken savedToken)
    {
        var authenticatedUser = new ClaimsPrincipal(new ClaimsIdentity(savedToken.Claims, TokenKey, ClaimTypes.Name, "Roles"));
        var authState = Task.FromResult(new AuthenticationState(authenticatedUser));

        await _localStorageService.SetItemAsync(TokenKey, savedToken.AuthenticationDetails.Token);
        await _localStorageService.SetItemAsync(ExpirationKey, savedToken.AuthenticationDetails.ExpirationDate);
        
        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", savedToken.AuthenticationDetails.Token);

        NotifyAuthenticationStateChanged(authState);
    }
    
    public async Task MarkUserAsLoggedOutAsync()
    {
        await _localStorageService.RemoveItemAsync(TokenKey);
        await _localStorageService.RemoveItemAsync(ExpirationKey);

        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", string.Empty);

        NotifyAuthenticationStateChanged(Task.FromResult(Anonymous));
    }

    private static SavedToken ParseToken(AuthenticationDetails incompleteDetails)
    {
        var (_, token, expirationDate) = incompleteDetails;

        var claims = JwtParser.ParseClaimsFromJwt(token).ToList();
        var userId = claims
            .First(x => x.Type.Equals(JwtRegisteredClaimNames.UniqueName, StringComparison.InvariantCultureIgnoreCase))
            .Value;

        return new SavedToken(claims, new AuthenticationDetails(userId, token, expirationDate));
    }
}