﻿namespace Client.Models.Competences;

public record SubjectSummary(string Id, string Name, int AreaCount, int CompetenceCount);