﻿using System.ComponentModel.DataAnnotations;

namespace Client.Models.Competences;

public record CompetenceArea(string Id, string Name, List<Competence> Competences);

public class CreateAreaModel
{
    [Required] public string Name { get; set; } = string.Empty;
}