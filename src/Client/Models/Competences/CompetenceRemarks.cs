﻿using System.ComponentModel.DataAnnotations;

namespace Client.Models.Competences;

public record CompetenceRemarks(string Id, string Content, User Author);

public class CreateRemarksModel
{
    [Required]
    public string Content { get; set; }
}