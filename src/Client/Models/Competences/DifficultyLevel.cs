﻿namespace Client.Models.Competences;

public enum DifficultyLevel
{
    Easy,
    Medium,
    Hard
}