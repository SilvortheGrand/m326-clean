﻿using System.ComponentModel.DataAnnotations;

namespace Client.Models.Competences;

public record CompetenceResource(string Id, string Name, string Url);

public class CreateResourceModel
{
    [Required]
    public string Name { get; set; }
    [Required]
    public string Url { get; set; }
}