﻿using System.ComponentModel.DataAnnotations;

namespace Client.Models.Competences;

public record Subject(string Id, string Name, List<CompetenceArea> Areas);

public class CreateSubjectModel
{
    [Required] public string Name { get; set; } = string.Empty;
}