﻿using System.ComponentModel.DataAnnotations;

namespace Client.Models.Competences;

public record Competence(string Id, string Name, string Description, DifficultyLevel DifficultyLevel,
    List<CompetenceResource> Resources, List<CompetenceRemarks> Remarks);

public class CreateCompetenceModel
{
    [Required] public string AreaId { get; set; }
    [Required] public string Name { get; set; }
    [Required] public string Description { get; set; }
    [Required] public DifficultyLevel DifficultyLevel { get; set; }
}