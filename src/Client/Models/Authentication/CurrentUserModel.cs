﻿namespace Client.Models.Authentication;

public record CurrentUserModel(string Id, string Username);