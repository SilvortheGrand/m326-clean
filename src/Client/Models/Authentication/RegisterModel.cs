﻿using System.ComponentModel.DataAnnotations;

namespace Client.Models.Authentication;

public class RegisterModel
{
    [Required] public string Username { get; set; } = null!;

    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; } = null!;

    [Required]
    [DataType(DataType.Password)]
    [Compare(nameof(Password))]
    public string PasswordConfirm { get; set; } = null!;

    [Required] [EmailAddress] public string EmailAddress { get; set; } = null!;
}