﻿namespace Client.Models;

public record User(string Id, string Username);