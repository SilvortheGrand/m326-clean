﻿using FluentResults;

namespace Client.Errors;

public class DuplicateUserError : Error
{
    public DuplicateUserError(string message = "User already exists.") : base(message)
    {
    }
}