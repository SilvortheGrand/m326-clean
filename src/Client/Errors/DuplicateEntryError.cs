﻿using FluentResults;

namespace Client.Errors;

public class DuplicateEntryError : Error
{
    public string DuplicateValue { get; }
    
    public DuplicateEntryError(string duplicateValue, string message) : base(message)
    {
        DuplicateValue = duplicateValue;
    }
}