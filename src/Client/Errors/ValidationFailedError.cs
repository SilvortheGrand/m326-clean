﻿using FluentResults;

namespace Client.Errors;

public class ValidationFailedError : Error
{
    public IEnumerable<string> ValidationErrors { get; }
    
    public ValidationFailedError(IEnumerable<string> validationErrors)
    {
        ValidationErrors = validationErrors;
    }
}