﻿using FluentResults;

namespace Client.Errors;

public class NotFoundError : Error
{
    public string NotFoundId { get; }
    public NotFoundError(string notFoundId, string message) : base(message)
    {
        NotFoundId = notFoundId;
    }
}