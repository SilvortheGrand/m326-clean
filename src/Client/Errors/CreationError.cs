﻿using FluentResults;

namespace Client.Errors;

public class CreationError : Error
{
    public CreationError(string message) : base(message)
    {
    }
}