﻿using FluentResults;

namespace Client.Errors;

public class UnauthorizedError : Error
{
    public UnauthorizedError(string message) : base(message)
    {
    }
}