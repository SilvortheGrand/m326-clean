using Blazored.LocalStorage;
using Client;
using Client.ApiClients;
using Client.ApiClients.Competences;
using Client.Authentication;
using Havit.Blazor.Components.Web;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Shared;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddAuthorizationCore();
builder.Services.AddTransient<IAuthenticationClient, AuthenticationClient>();
builder.Services.AddSingleton<RefreshTokenScheduler>();
builder.Services.AddScoped<AuthenticationStateProvider, JwtAuthenticationStateProvider>();
builder.Services.AddScoped<IJwtAuthenticationStateProvider, JwtAuthenticationStateProvider>(opt =>
    opt.GetRequiredService<JwtAuthenticationStateProvider>());
builder.Services.AddScoped<IJwtAuthenticationService, JwtAuthenticationService>();
builder.Services.AddScoped<CurrentUserService>();

builder.Services.AddAutoMapper(typeof(Program).Assembly);

builder.Services.AddTransient<ICompetenceManagementClient, CompetenceManagementClient>();
builder.Services.AddTransient<ICompetenceClient, CompetenceClient>();

builder.Services.AddBlazoredLocalStorage();

builder.Services.AddHxServices();
builder.Services.AddHxMessageBoxHost();

builder.Services.AddSingleton(_ => new HttpClient {BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)});

await builder.Build().RunAsync();