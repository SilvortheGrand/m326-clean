﻿using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Client.Helpers;

public static class HttpHelpers
{
    private static readonly JsonSerializerOptions DefaultOptions = new()
        { PropertyNameCaseInsensitive = true, Converters = { new JsonStringEnumConverter() } };

    public static HttpContent CreateStringContent(object body,
        JsonSerializerOptions? options = null,
        Encoding? encoding = null,
        string mediaType = "application/json")
    {
        return new StringContent(
            JsonSerializer.Serialize(body, options ?? DefaultOptions),
            encoding ?? Encoding.UTF8, mediaType);
    }

    public static async Task<TContent?> DeserializeContentAsync<TContent>(this HttpResponseMessage response,
        JsonSerializerOptions? options = null)
    {
        var content = await response.Content.ReadAsStringAsync();
        return JsonSerializer.Deserialize<TContent>(content,
            options ?? DefaultOptions);
    }
}