﻿using System.Security.Claims;
using System.Text.Json;

namespace Client.Helpers;

public static class JwtParser
{
    public static IEnumerable<Claim> ParseClaimsFromJwt(string token)
    {
        var claims = new List<Claim>();
        var payload = token.Split('.')[1];

        var jsonBytes = ParseBase64WithoutPadding(payload);

        var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);

        if(TryGetRoles(keyValuePairs!, out var roleClaim))
            claims.Add(roleClaim);
            
        claims.AddRange(keyValuePairs!.Select(x => new Claim(x.Key, x.Value.ToString()!)));

        return claims;
    }

    private static bool TryGetRoles(Dictionary<string, object> keyValuePairs, out Claim roleClaim)
    {
        var claim = keyValuePairs.SingleOrDefault(x => x.Key.Equals("role"));
        if (string.IsNullOrEmpty(claim.Key))
        {
            roleClaim = new Claim(ClaimsIdentity.DefaultRoleClaimType, "");
            return false;
        }

        keyValuePairs.Remove(claim.Key);
        roleClaim = new Claim(ClaimsIdentity.DefaultRoleClaimType, claim.Value.ToString()!);
        return true;
    }

    private static byte[] ParseBase64WithoutPadding(string payload)
    {
        switch (payload.Length % 4)
        {
            case 2:
                payload += "==";
                break;
            case 3:
                payload += "=";
                break;
        }

        return Convert.FromBase64String(payload);
    }
}