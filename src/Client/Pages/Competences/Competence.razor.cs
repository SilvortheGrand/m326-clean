﻿using Microsoft.AspNetCore.Components;

namespace Client.Pages.Competences;

public partial class Competence : CompetenceManagementComponent, IComponent
{
    [Parameter] public string AreaId { get; set; }
    [Parameter] public Models.Competences.Competence CompetenceInstance { get; set; }
    [Parameter] public EventCallback<CompetenceDeletedArgs> OnDelete { get; set; }

    protected override void OnParametersSet()
    {
        Parent.Register(this);
    }

    private async Task DeleteAsync()
    {
        await CompetenceManagementClient.DeleteCompetenceAsync(CompetenceInstance.Id);
        await OnDelete.InvokeAsync(new CompetenceDeletedArgs(AreaId, CompetenceInstance.Id));
    }

    [Parameter]
    public IComponentParent Parent { get; set; } = null!;
    public void Refresh()
    {
        StateHasChanged();
    }

    public Task TriggerFetchAsync()
    {
        return Task.CompletedTask;
    }
}