﻿using Client.Models.Competences;
using Havit.Blazor.Components.Web;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Competences;

public partial class CompetenceRemark : IComponent
{
    private CompetenceRemarks? Remark { get; set; }
    [Parameter] public string RemarkId { get; set; } = string.Empty;
    
    [Parameter] public EventCallback OnDelete { get; set; }

    protected override async Task OnInitializedAsync()
    {
        Parent.Register(this);
        
        if (Remark is null && !string.IsNullOrEmpty(RemarkId))
        {
            var result = await CompetenceClient.GetRemarksAsync(RemarkId);
            if (result.IsFailed)
            {
                await MessageBoxService.ShowAsync(new MessageBoxRequest
                {
                    Title = "Remark not found",
                    Text = "Could not find remarks!",
                    Buttons = MessageBoxButtons.Ok
                });
                return;
            }

            Remark = result.Value;
        }
    }

    private async Task DeleteAsync()
    {
        var result = await CompetenceClient.DeleteRemarkAsync(Remark!.Id);
        if (result.IsFailed)
            await MessageBoxService.ShowAsync(new MessageBoxRequest
            {
                Title = "Error",
                Text = "Could not delete remarks",
                Buttons = MessageBoxButtons.Ok
            });
        else
        {
            await OnDelete.InvokeAsync();
        }
    }

    [Parameter]
    public IComponentParent Parent { get; set; }
    public void Refresh()
    {
        StateHasChanged();
    }

    public Task TriggerFetchAsync()
    {
        return OnInitializedAsync();
    }
}