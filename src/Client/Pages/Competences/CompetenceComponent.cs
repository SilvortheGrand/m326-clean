﻿using Client.ApiClients.Competences;
using Client.Authentication;
using Havit.Blazor.Components.Web;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Competences;

public abstract class CompetenceComponent : ComponentBase
{
    [Inject] public ICompetenceClient CompetenceClient { get; set; } = null!;
    [Inject] public CurrentUserService CurrentUserService { get; set; } = null!;
    [Inject] public NavigationManager NavigationManager { get; set; } = null!;
    [Inject] public IHxMessageBoxService MessageBoxService { get; set; } = null!;
}