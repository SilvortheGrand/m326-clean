﻿namespace Client.Pages.Competences;

public record CompetenceDeletedArgs(string AreaId, string CompetenceId);