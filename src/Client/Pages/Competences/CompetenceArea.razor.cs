﻿using Havit.Blazor.Components.Web;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Competences;

public partial class CompetenceArea : CompetenceManagementComponent, IComponent, IComponentParent
{
    private Models.Competences.CompetenceArea? Area { get; set; }

    [Parameter] public IComponentParent Parent { get; set; } = null!;
    public List<IComponent> Children { get; set; } = new();

    public void Refresh()
    {
        foreach (var child in Children) 
            child.Refresh();
    }

    public Task TriggerFetchAsync()
    {
        return OnParametersSetAsync();
    }

    public void Register(IComponent component)
    {
        if(!Children.Contains(component))
            Children.Add(component);
    }

    void IComponent.Refresh()
    {
        Refresh();
        StateHasChanged();
    }

    [Parameter] public string? AreaId { get; set; }
    [Parameter] public EventCallback<string> OnDeleteArea { get; set; }
    [Parameter] public EventCallback<CompetenceDeletedArgs> OnDeleteCompetence { get; set; }
    [Parameter] public EventCallback<string> OnClickCompetence { get; set; }
    
    protected override async Task OnParametersSetAsync()
    {
        Parent.Register(this);
        
        if (!string.IsNullOrEmpty(AreaId))
        {
            var result = await CompetenceClient.GetAreaAsync(AreaId);
            if (result.IsFailed)
            {
                await MessageBoxService.ShowAsync(new MessageBoxRequest
                {
                    Title = "Area not found",
                    Text = "Could not find area!",
                    Buttons = MessageBoxButtons.Ok
                });
                return;
            }

            Area = result.Value;
        }
    }

    private Task ClickCompetence(string id)
    {
        return OnClickCompetence.InvokeAsync(id);
    }
    
    private async Task DeleteCompetenceAsync(CompetenceDeletedArgs args)
    {
        await OnDeleteCompetence.InvokeAsync(args);
        Area!.Competences.RemoveAll(x => x.Id.Equals(args.CompetenceId));
    }

    private async Task DeleteAreaAsync()
    {
        await CompetenceManagementClient.DeleteAreaAsync(Area!.Id);
        await OnDeleteArea.InvokeAsync(Area!.Id);
    }
}