﻿namespace Client.Pages.Competences;

public interface IComponent
{
    public IComponentParent Parent { get; set; }
    void Refresh();
    Task TriggerFetchAsync();
}

public interface IComponentParent
{
    protected List<IComponent> Children { get; set; }
    void Refresh();
    void Register(IComponent component);
}