﻿using Client.Models.Competences;
using Havit.Blazor.Components.Web;
using Havit.Blazor.Components.Web.Bootstrap;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Competences;

public partial class CompetenceSideView : CompetenceManagementComponent, IComponent, IComponentParent
{
    private Models.Competences.Competence? CompetenceInstance { get; set; }
    [Parameter] public string CompetenceId { get; set; } = string.Empty;

    private string _lastCompetenceId = string.Empty;

    protected override async Task OnInitializedAsync()
    {
        Parent.Register(this);

        if (!string.IsNullOrEmpty(CompetenceId) && _lastCompetenceId != CompetenceId)
        {
            var result = await CompetenceClient.GetCompetenceAsync(CompetenceId);
            if (result.IsFailed)
            {
                await MessageBoxService.ShowAsync(new MessageBoxRequest
                {
                    Title = "Competence not found",
                    Text = "Could not find competence!",
                    Buttons = MessageBoxButtons.Ok
                });
                return;
            }

            CompetenceInstance = result.Value;
            _lastCompetenceId = result.Value.Id;
        }
    }

    [Parameter] public IComponentParent Parent { get; set; } = null!;

    public List<IComponent> Children { get; set; } = new();

    private HxModal _createResourceModal;
    private CreateResourceModel _createResourceModel = new();

    private HxModal _createRemarksModal;
    private CreateRemarksModel _createRemarksModel = new();

    private async Task CreateResourceAsync()
    {
        await CompetenceManagementClient.CreateResourceAsync(CompetenceId, _createResourceModel.Name,
            _createResourceModel.Url);
        await CloseResourceModalAsync();
        await ForceFetch();
    }

    private async Task CreateRemarkAsync()
    {
        await CompetenceManagementClient.CreateRemarksAsync(CompetenceId, _createRemarksModel.Content);
        await CloseRemarksModalAsync();
        await ForceFetch();
    }

    private async Task OpenAddResourceModalAsync()
    {
        _createResourceModel = new CreateResourceModel();
        await _createResourceModal.ShowAsync();
    }

    private async Task OpenAddRemarkModalAsync()
    {
        _createRemarksModel = new CreateRemarksModel();
        await _createRemarksModal.ShowAsync();
    }

    private async Task CloseResourceModalAsync()
    {
        await _createResourceModal.HideAsync();
    }

    private async Task CloseRemarksModalAsync()
    {
        await _createRemarksModal.HideAsync();
    }

    public void Refresh()
    {
        StateHasChanged();
    }

    public void Register(IComponent component)
    {
        if (!Children.Contains(component))
            Children.Add(component);
    }

    public Task TriggerFetchAsync()
    {
        return OnInitializedAsync();
    }

    private async Task ForceFetch()
    {
        foreach (var component in Children) await component.TriggerFetchAsync();
        _lastCompetenceId = string.Empty;
        await OnInitializedAsync();
    }
}