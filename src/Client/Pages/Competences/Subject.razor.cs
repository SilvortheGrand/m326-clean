﻿using Client.Models.Competences;
using Havit.Blazor.Components.Web;
using Havit.Blazor.Components.Web.Bootstrap;
using Microsoft.AspNetCore.Components;
using Shared;

namespace Client.Pages.Competences;

public partial class Subject : CompetenceManagementComponent, IComponentParent
{
    [Parameter] public string SubjectId { get; set; } = string.Empty;

    private Models.Competences.Subject? _subject;

    private HxModal _createAreaModal;
    private CreateAreaModel _createAreaModel = new();
    
    private HxModal _createCompetenceModal;
    private CreateCompetenceModel _createCompetenceModel = new();

    private readonly List<string> _createErrors = new();

    private string _currentShownCompetenceId = string.Empty;
    
    protected override async Task OnParametersSetAsync()
    {
        var result = await CompetenceClient.GetSubjectAsync(SubjectId);
        if (result.IsFailed)
        {
            await MessageBoxService.ShowAsync(new MessageBoxRequest
            {
                Title = "Subject not found",
                Text = "Could not find subject!",
                Buttons = MessageBoxButtons.Ok
            });

            NavigationManager.NavigateTo("/");
            return;
        }

        _subject = result.Value;
    }

    private async Task HandleClickCompetence(string id)
    {
        if(_currentShownCompetenceId != id)
            _currentShownCompetenceId = id;
        
        foreach (var child in Children) await child.TriggerFetchAsync();
    }
    
    private async Task CreateAreaAsync()
    {
        _createErrors.Clear();
        var result = await CompetenceManagementClient.CreateAreaAsync(SubjectId, _createAreaModel.Name);
        if (result.IsFailed)
            _createErrors.AddRange(result.Errors.Select(x => x.Message));
        else
        {
            await CloseCreateAreaModalAsync();
            await OnParametersSetAsync();
        }
    }
    
    private async Task CreateCompetenceAsync()
    {
        _createErrors.Clear();
        var result =
            await CompetenceManagementClient.CreateCompetenceAsync(_createCompetenceModel.AreaId,
                _createCompetenceModel.Name, _createCompetenceModel.Description, _createCompetenceModel.DifficultyLevel);
        if (result.IsFailed)
            _createErrors.AddRange(result.Errors.Select(x => x.Message));
        else
        {
            await CloseCreateCompetenceModalAsync();
            foreach (var child in Children) await child.TriggerFetchAsync();
        }
    }

    private void AreaDeleted(string id)
    {
        _subject!.Areas.RemoveAll(x => x.Id.Equals(id));
    }

    private async Task DeletedCompetenceAsync(CompetenceDeletedArgs args)
    {
        _subject!.Areas.Single(x => x.Id.Equals(args.AreaId)).Competences
            .RemoveAll(x => x.Id.Equals(args.CompetenceId));
        
        foreach (var child in Children) await child.TriggerFetchAsync();
    }
    
    private async Task OpenCreateAreaModal()
    {
        _createAreaModel = new CreateAreaModel();
        await _createAreaModal.ShowAsync();
    }

    private async Task CloseCreateAreaModalAsync()
    {
        await _createAreaModal.HideAsync();
    }
    
    private async Task OpenCreateCompetenceModal()
    {
        _createCompetenceModel = new CreateCompetenceModel();
        await _createCompetenceModal.ShowAsync();
    }

    private async Task CloseCreateCompetenceModalAsync()
    {
        await _createCompetenceModal.HideAsync();
    }

    public List<IComponent> Children { get; set; } = new();
    public void Refresh()
    {
        foreach (var child in Children) child.Refresh();
    }

    public void Register(IComponent component)
    {
        if(!Children.Contains(component))
            Children.Add(component);
    }
}