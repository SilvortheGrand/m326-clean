﻿using Havit.Blazor.Components.Web;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Competences;

public partial class CompetenceResource : CompetenceManagementComponent, IComponent
{
    private Models.Competences.CompetenceResource? Resource { get; set; }
    [Parameter] public string ResourceId { get; set; } = string.Empty;
    [Parameter] public EventCallback OnDelete { get; set; }

    protected override async Task OnInitializedAsync()
    {
        Parent.Register(this);
        
        if (Resource is null || !string.IsNullOrEmpty(ResourceId))
        {
            var result = await CompetenceClient.GetResourceAsync(ResourceId);
            if (result.IsFailed)
            {
                await MessageBoxService.ShowAsync(new MessageBoxRequest
                {
                    Title = "Resource not found",
                    Text = "Could not find resource!",
                    Buttons = MessageBoxButtons.Ok
                });
                return;
            }

            Resource = result.Value;
        }
    }

    private async Task DeleteAsync()
    {
        await CompetenceManagementClient.DeleteResourceAsync(Resource!.Id);
        await OnDelete.InvokeAsync();
    }

    [Parameter]
    public IComponentParent Parent { get; set; }
    public void Refresh()
    {
        StateHasChanged();
    }

    public Task TriggerFetchAsync()
    {
        return OnInitializedAsync();
    }
}