﻿using Client.ApiClients.Competences;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Competences;

public abstract class CompetenceManagementComponent : CompetenceComponent
{
    [Inject] public ICompetenceManagementClient CompetenceManagementClient { get; set; } = null!;
}