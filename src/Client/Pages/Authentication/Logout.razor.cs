﻿using Client.Authentication;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Authentication;

public partial class Logout
{
    [Inject] public IJwtAuthenticationService JwtAuthenticationService { get; set; } = null!;
    [Inject] public NavigationManager NavigationManager { get; set; } = null!;

    protected override async Task OnInitializedAsync()
    {
        await JwtAuthenticationService.LogoutAsync();
        NavigationManager.NavigateTo("/");
    }
}