﻿using Client.Authentication;
using Client.Errors;
using Client.Models.Authentication;
using Microsoft.AspNetCore.Components;

namespace Client.Pages.Authentication;

public partial class Register
{
    [Inject] public IJwtAuthenticationService JwtAuthenticationService { get; set; } = null!;
    [Inject] public NavigationManager NavigationManager { get; set; } = null!;

    private RegisterModel _model = new();

    private readonly List<string> _registerErrorMessages = new();

    private async Task RegisterAsync()
    {
        _registerErrorMessages.Clear();

        var result = await JwtAuthenticationService.RegisterAsync(_model);

        if (result.IsFailed)
        {
            var errors = result.Errors.Where(x => x is ValidationFailedError)
                .SelectMany(x => ((ValidationFailedError) x).ValidationErrors).ToList();
            errors.AddRange(result.Errors.Where(x => x is not ValidationFailedError).Select(x => x.Message));

            _registerErrorMessages.AddRange(errors);
        }
        else
            NavigationManager.NavigateTo("/");
    }
}