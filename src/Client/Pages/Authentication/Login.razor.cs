﻿using Client.Authentication;
using Client.Models.Authentication;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;

namespace Client.Pages.Authentication;

public partial class Login
{
    [Inject] public IJwtAuthenticationService JwtAuthenticationService { get; set; } = null!;
    [Inject] public NavigationManager NavigationManager { get; set; } = null!;

    private LoginModel _model = new();

    private readonly List<string> _loginErrorMessages = new();
    private string _returnUrl = string.Empty;

    protected override void OnInitialized()
    {
        var uri = NavigationManager.ToAbsoluteUri(NavigationManager.Uri);
        if (QueryHelpers.ParseQuery(uri.Query).TryGetValue("returnUrl", out var url))
            _returnUrl = url.First()!;
    }

    private async Task LoginAsync()
    {
        _loginErrorMessages.Clear();

        var result = await JwtAuthenticationService.LoginAsync(_model);
        if (result.IsFailed)
            _loginErrorMessages.AddRange(result.Errors.Select(x => x.Message));
        else
            NavigationManager.NavigateTo(string.IsNullOrEmpty(_returnUrl) ? "/" : _returnUrl, true);
    }
}