﻿using Client.Models.Competences;
using Client.Pages.Competences;
using Havit.Blazor.Components.Web.Bootstrap;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace Client.Pages;

public partial class Index : CompetenceManagementComponent
{
    private readonly List<SubjectSummary> _subjects = new();
    private readonly List<string> _createErrors = new();

    private HxModal _createSubjectModal = null!;
    private CreateSubjectModel _model = new();

    protected override async Task OnInitializedAsync()
    {
        _subjects.Clear();
        var result = await CompetenceClient.GetSubjectOverviewAsync();
        if(result.IsFailed)
            NavigationManager.NavigateToLogin("logout");
        else
            _subjects.AddRange(result.Value);
    }

    private void NavigateToSubject(string id)
    {
        NavigationManager.NavigateTo($"/subject/{id}");
    }

    private async Task ShowCreateModalAsync()
    {
        _model = new CreateSubjectModel();
        await _createSubjectModal.ShowAsync();
    }

    private async Task CreateNewSubjectAsync()
    {
        _createErrors.Clear();
        var result = await CompetenceManagementClient.CreateSubjectAsync(_model.Name);
        
        if (!result.IsFailed)
            NavigationManager.NavigateTo($"/subject/{result.Value.Id}");
        else
            _createErrors.AddRange(result.Errors.Select(x => x.Message));
    }

    private async Task DeleteSubjectAsync(string id)
    {
        await CompetenceManagementClient.DeleteSubjectAsync(id);
        await OnInitializedAsync();
    }
}