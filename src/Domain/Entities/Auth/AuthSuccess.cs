﻿namespace Domain.Entities.Auth;

public record AuthSuccess(string Token, string RefreshToken);