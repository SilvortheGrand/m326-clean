﻿namespace Domain.Entities.Competences;

public record DomainSubjectInfo(Guid Id, string Name, int AreaCount, int CompetenceCount);
public record DomainSubject(Guid Id, string Name, List<DomainCompetenceArea> Areas);

public record DomainCompetenceArea(Guid Id, string Name, List<DomainCompetence> Competences);

public record DomainCompetence(Guid Id, string Name, string Description, DomainDifficultyLevel DifficultyLevel, List<DomainCompetenceResource> Resources,
    List<User> CompletedUsers, List<DomainCompetenceRemarks> Remarks);

public record DomainCompetenceResource(Guid Id, string Name, string Url);

public record DomainCompetenceRemarks(Guid Id, string Content, User Author);