﻿namespace Domain.Entities.Competences;

public enum DomainDifficultyLevel
{
    Easy,
    Medium,
    Hard
}