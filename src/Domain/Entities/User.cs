﻿using Domain.Entities.Competences;

namespace Domain.Entities;

public class User
{
    public Guid Id { get; set; }
    public string Username { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public List<DomainCompetenceRemarks> Remarks { get; set; } = new();
    public List<DomainCompetence> CompletedCompetences { get; set; } = new();
}