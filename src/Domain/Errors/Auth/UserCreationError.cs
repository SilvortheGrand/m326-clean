﻿using FluentResults;

namespace Domain.Errors.Auth;

public class UserCreationError : Error
{
    public UserCreationError(string errorDescription)
    {
        Message = errorDescription;
    }
}