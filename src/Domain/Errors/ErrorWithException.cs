﻿using FluentResults;

namespace Domain.Errors;

public abstract class ErrorWithException : Error
{
    public Exception? Exception { get; }

    public ErrorWithException(string message) : base(message)
    {
    }

    public ErrorWithException(Exception exception)
    {
        Exception = exception;
    }
}