﻿using FluentResults;

namespace Domain.Errors.Competences;

public class CreationError<T> : ErrorWithException
{
    public CreationError(string message) : base(message)
    {
    }

    public CreationError(Exception exception) : base(exception)
    {
    }
}