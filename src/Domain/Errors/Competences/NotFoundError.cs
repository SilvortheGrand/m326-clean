﻿using FluentResults;

namespace Domain.Errors.Competences;

public class NotFoundError<T> : Error
{
    public NotFoundError(string message) : base(message) 
    {
    }
}