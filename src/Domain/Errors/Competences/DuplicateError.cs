﻿using FluentResults;

namespace Domain.Errors.Competences;

public class DuplicateError<T> : Error
{
    public DuplicateError(string message) : base(message)
    {
    }
}