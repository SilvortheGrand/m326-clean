﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.IIS;
using Server.Extensions;

namespace Server.Tests;

public class HttpContextExtensionsTests
{
    private const string RefreshTokenCookieName = "refreshToken";

    [Fact]
    public void SetRefreshTokenCookie_SetsResponseRefreshTokenCookie_WithCorrectRefreshTokenName()
    {
        var response = new DefaultHttpContext().Response;
        const string myRefreshToken = "Some testing token";
        
        response.SetRefreshTokenCookie(myRefreshToken);

        response.Headers.Should().Contain(x => x.Key.Equals("Set-Cookie") && x.Value.Any(z => z.Contains(RefreshTokenCookieName)));
    }

    [Fact]
    public void RemoveRefreshTokenCookie_RemovesResponseRefreshTokenCookie_WithSetRefreshTokenName()
    {
        var response = new DefaultHttpContext().Response;
        const string myRefreshToken = "Some testing token";
        response.SetRefreshTokenCookie(myRefreshToken);
        
        response.RemoveRefreshTokenCookie();
        
        response.Headers.Should().NotContain(x => x.Key.Equals("Set-Cookie") && x.Value.Any(z => !z.Contains($"{RefreshTokenCookieName}=;")));
    }
}